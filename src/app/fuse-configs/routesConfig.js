import React from 'react';
import { Redirect } from 'react-router-dom';
import { FuseUtils } from '@fuse/index';
//import { appsConfigs } from 'app/main/apps/appsConfigs';
import { pagesConfigs } from 'app/main/pages/pagesConfigs';
import { authRoleExamplesConfigs } from 'app/main/auth/authRoleExamplesConfigs';
import { UserInterfaceConfig } from 'app/main/user-interface/UserInterfaceConfig';
//import { DocumentationConfig } from 'app/main/documentation/DocumentationConfig';
import { LoginConfig } from 'app/main/login/LoginConfig';
//import { RegisterConfig } from 'app/main/register/RegisterConfig';
import { LogoutConfig } from 'app/main/logout/LogoutConfig';
import { CallbackConfig } from 'app/main/callback/CallbackConfig';
import { AdminConfig } from 'app/main/admin/AdminConfig';
//import { UserConfig } from 'app/main/user/UserConfig';
import { ProductConfig } from 'app/main/product/ProductConfig';
// import { ContactsAppConfig } from 'app/main/redemption/ContactsAppConfig';
import { MappingConfig } from 'app/main/mapping/MappingConfig';
import { MarketplaceConfig } from 'app/main/marketplace/MarketplaceConfig';
// import {ProjectDashboardAppConfig} from 'app/main/dashboards/project/ProjectDashboardAppConfig';

const routeConfigs = [
    //...appsConfigs,
    ...pagesConfigs,
    ...authRoleExamplesConfigs,
    UserInterfaceConfig,
    //DocumentationConfig,
    LogoutConfig,
    LoginConfig,
    //RegisterConfig,
    LogoutConfig,
    CallbackConfig,
    AdminConfig,
    //UserConfig,
    ProductConfig,
    // ContactsAppConfig,
    MappingConfig,
    MarketplaceConfig,
    // ProjectDashboardAppConfig
];

const routes = [
    //if you want to make whole app auth protected by default change defaultAuth for example:
    // ...FuseUtils.generateRoutesFromConfigs(routeConfigs, ['admin','staff','user']),
    // The individual route configs which has auth option won't be overridden.
    // ...FuseUtils.generateRoutesFromConfigs(routeConfigs, null),
    // {
    //     path     : '/',
    //     exact    : true,
    //     component: () => <Redirect to="/apps/dashboards/analytics"/>
    // },
    // {
    //     component: () => <Redirect to="/pages/errors/error-404"/>
    // }
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs, ['admin']),
    {
        path: '/',
        exact: true,
        component: () => <Redirect to="/login" />
    },
    {
        component: () => <Redirect to="/pages/errors/error-404" />
    }
];

export default routes;
