// import { MaterialUIComponentsNavigation } from 'app/main/documentation/material-ui-components/MaterialUIComponentsNavigation';
// import { authRoles } from 'app/auth';

const navigationConfig = [
    {
        'id'      : 'dashboards',
        'title'   : 'Dashboards',
        'type'    : 'item',
        'icon'    : 'dashboard',
        'url'  : '/apps/dashboards/project'
    },
    {
        'id'      : 'catalog',
        'title'   : 'Catalog',
        'type'    : 'collapse',
        'icon'    : 'ballot',        
        'children': [
            {
                // 
                // {
                //     'id': 'product',
                //     'title': 'Product',
                //     'type': 'item',
                //     'icon': 'shopping_basket',
                //     'url': '/product/all',
                //     'exact': true
                // }
                'id'   : 'products',
                'title': 'Products',
                // 'icon': 'shopping_basket',
                'type' : 'item',                
                'url'  : '/product/all',///catalog/product/all
                'exact': true
            },
            {
                'id'   : 'bundled products',
                'title': 'Bundled Products',
                'type' : 'item',
                'url'  : '/catalog/bundledproducts',
                'exact': true
            },
            {
                'id'   : 'categories',
                'title': 'Categories',
                'type' : 'item',
                'url'  : '/catalog/products',
                'exact': true
            }
        ]
    },
    {
        'id'      : 'promotions',
        'title'   : 'Promotions',
        'type'    : 'collapse',
        'icon'    : 'local_offer',
        'url'     : '/promotions',
        'children': [
            {
                'id'   : 'price schedule',
                'title': 'Price Schedule',
                'type' : 'item',
                'url'  : '/promotions/priceSchedule'                
            },
            {
                'id'   : 'flash sale',
                'title': 'Flash Sale',
                'type' : 'item',
                'url'  : '/promotions/flashSale'                
            },
            {
                'id'   : 'buyxgety',
                'title': 'Buy x get y',
                'type' : 'item',
                'url'  : '/promotions/buyxgety'                
            },
            {
                'id'   : 'gwp',
                'title': 'GWP',
                'type' : 'item',
                'url'  : '/promotions/gwp'                
            }
        ]        
    },    
    {
        'id'      : 'sales',
        'title'   : 'Sales',
        'type'    : 'collapse',
        'icon'    : 'local_mall',
        'url'     : '/apps/sales',
        'children': [
            {
                'id'   : 'orders',
                'title': 'Orders',
                'type' : 'item',
                'url'  : '/apps/sales/orders'                
            }
        ]
    },
    {
        'id'      : 'inventory',
        'title'   : 'Inventory',
        'type'    : 'collapse',
        'icon'    : 'account_balance',
        'url'     : '/apps/inventory'        
    },
    {
        'id'      : 'custom rules',
        'title'   : 'Custom Rules',
        'type'    : 'item',
        'icon'    : 'label_off',
        'url'     : '/apps/customRule'        
    },
    {
        'id'      : 'reports',
        'title'   : 'Reports',
        'type'    : 'item',
        'icon'    : 'folder_special',
        'url'     : '/apps/customRule'        
    },
    {
        'id'      : 'settings',
        'title'   : 'Settings',
        'type'    : 'collapse',
        'icon'    : 'settings_applications',
        'url'     : '/setting',
        'children': [
            {
                'id'   : 'erps',
                'title': 'ERPs',
                'type' : 'item',
                'url'  : '/apps/integration/erps'                
            },
            {
                'id'   : 'online channels',
                'title': 'Online Channels',
                'type' : 'item',
                'url'  : '/fieldMapping/overallMapping' ///fieldMapping/overallMapping               
            },
            {
                'id'   : 'offline channels',
                'title': 'Offline Channels',
                'type' : 'item',
                'url'  : '/apps/integration/offlineChannels'                
            },
            {
                'id'   : 'shipping methods',
                'title': 'Shipping Methods',
                'type' : 'item',
                'url'  : '/apps/integration/shipping Methods'                
            },
            {
                'id'   : 'user',
                'title': 'Users',
                'type' : 'item',
                'url'  : '/admin'
            },
            {
                'id'   : 'marketplaceMapping',
                'title': 'Marketplace Mapping',
                'type' : 'item',
                'url'  : '/fieldMapping/createDataSource/:marketplace/:storeId/:storeName?',
                'exact': true                        
            },
            {
                'id'   : 'field-mapping2',
                'title': 'Store Mapping',
                'type' : 'item',   
                'url'  : '/fieldMapping/store',
                'exact': true   
            },
            {
                'id'   : 'allMarketplace',
                'title': 'Marketplaces',
                'type' : 'item',
                'url'  : '/marketplace/all',
                'exact': true                        
            },
        ]          
    },     
    // {
    //     'id': 'mapping',
    //     'title': 'Mapping',
    //     'type': 'collapse',
    //     'icon' : 'import_contacts',        
    //     'children': [                    
    //                 {
    //                     'id'   : 'marketplaceMapping',
    //                     'title': 'Marketplace Mapping',
    //                     'type' : 'item',
    //                     'url'  : '/fieldMapping/createDataSource/:marketplace/:storeId/:storeName?',
    //                     'exact': true                        
    //                 },
    //                 {
    //                     'id'   : 'field-mapping2',
    //                     'title': 'Store Mapping',
    //                     'type' : 'item',   
    //                     'url'  : '/fieldMapping/store',
    //                     'exact': true   
    //                 }
    //     ]
    // },
    // {
    //     'id': 'marketplace',
    //     'title': 'Marketplace',
    //     'type': 'collapse',
    //     'icon' : 'money',
    //     // 'url': '/fieldMapping',
    //     'children': [
    //                 {
    //                     'id'   : 'allMarketplace',
    //                     'title': 'Marketplaces',
    //                     'type' : 'item',
    //                     'url'  : '/marketplace/all',
    //                     'exact': true                        
    //                 }
                  
    //             ]

    // }
    
];

export default navigationConfig;
