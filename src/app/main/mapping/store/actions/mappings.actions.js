import axios from 'axios';

export const GET_STORES = 'GET PRODUCTS';
export const SET_STORES_SEARCH_TEXT = 'SET STORES SEARCH TEXT';

export function getStores(param)
{
    const request = axios.get('/api/test-app/store'
    // ,{        
    //         "query": [    
    //             {
    //                 "fn": "getProductList",
    //                 "param": {
    //                     "offset": param.offset,
    //                     "limit": param.limit,
    //                     "orderKey": ["sku"], 
    //                     "order": ["asc"],
    //                     "filter": []//param.filter
    //                 }
    //             }
    //         ],
    //         "token": localStorage.getItem('jwt_access_token'),
    //         "New": true
        
    //   }
      );

    return (dispatch) =>
        request.then((response) =>{
            console.log("succes response=",response.data);
            dispatch({
                type   : GET_STORES,
                payload: response.data
            })
        }
           
        );
}

export function setStoresSearchText(event)
{
    return {
        type      : SET_STORES_SEARCH_TEXT,
        searchText: event.target.value
    }
}

