import axios from 'axios';
import Api from "../../../../store/api";
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';

export const GET_STORE = 'GET_STORE';
export const SAVE_MAPPING = 'SAVE_MAPPING';
export const GET_MAPPING = 'GET_MAPPING';
export const GET_MARKET_FIELD = 'GET_MARKET_FIELD';
export const GET_OWN_FIELD = 'GET_OWN_FIELD';

// export const GET_STORE_TAB = 'GET PRODUCT TAB';
// export const GET_STORE_CONTENT = 'GET PRODUCT CONTENT';

//call from api
export function getStore(param)
{
    const request = axios.get('/api/test-app/store_a'
    // , 
    // {        
    //         "query": [    
    //             {
    //                 "fn": "getProduct",
    //                 "param": {
    //                     ...param
    //                 }
    //             }
    //         ],
    //         "token": localStorage.getItem('jwt_access_token'),
    //         "New": true    
    // }
    );

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_STORE,
                payload: response.data
            })
        );
}

//call set form
export function getMarketField(marketplace)
{
    
    let url = '/api/test-app/';
    const request = axios.get(url + marketplace);

    return (dispatch) =>
        request.then((response) =>{            
            dispatch({
                type   : GET_MARKET_FIELD,
                payload: response.data
            })
            console.log(" res=",response.data)
            
        }
            
        ).catch(err=>{
            console.log("getMarketfLD=",err);

            dispatch({
                type   : GET_MARKET_FIELD,
                payload: null
            })
        });
}

export function getOwnField()
{
    const request = axios.get('/api/test-app/ownField');

    return (dispatch) =>
        request.then((response) =>{            
            dispatch({
                type   : GET_OWN_FIELD,
                payload: response.data
            })
            console.log(" res own=",response.data)
        }
            
        );
}

export function getMapping()
{
    const request = axios.get('/api/test-app/mapField');

    return (dispatch) =>
        request.then((response) =>{            
            dispatch({
                type   : GET_MAPPING,
                payload: response.data
            })
            console.log(" res own=",response.data[0])
        }
            
        );
}
// export function getProductTab()
// {
//     const request = axios.get('/api/test-app/product_tabs');

//     return (dispatch) =>
//         request.then((response) =>{            
//             dispatch({
//                 type   : GET_STORE_TAB,
//                 payload: response.data
//             })
//         }
            
//         );
// }

// export function getProductContent()
// {
//     const request = axios.get('/api/test-app/product_tabs');

//     return (dispatch) =>
//         request.then((response) =>{
//             console.log(" res=",response.data);
//             dispatch({
//                 type   : GET_STORE_CONTENT,
//                 payload: response.data
//             })
//         }
            
//         );
// }

export function saveMapping(data)
{
    const request = axios.post('/api/e-commerce-app/product/save', data);

    console.log("data=",data)

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Store Saved'}));

                return dispatch({
                    type   : SAVE_MAPPING,
                    payload: response.data
                })
            }
        );
}

export function newStore()
{
    const data = {
        id              : FuseUtils.generateGUID(),
        name            : '',
        handle          : '',
        description     : '',
        categories      : [],
        tags            : [],
        images          : [],
        priceTaxExcl    : 0,
        priceTaxIncl    : 0,
        taxRate         : 0,
        comparedPrice   : 0,
        quantity        : 0,
        sku             : '',
        width           : '',
        height          : '',
        depth           : '',
        weight          : '',
        extraShippingFee: 0,
        active          : true
    };

    return {
        type   : GET_STORE,
        payload: data
    }
}
