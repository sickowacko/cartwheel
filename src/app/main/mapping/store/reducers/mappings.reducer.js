import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const storesReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_STORES:
        {   console.log("action.payload=",action.payload)
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_STORES_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default storesReducer;
