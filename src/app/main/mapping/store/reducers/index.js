import {combineReducers} from 'redux';
import mappings from './mappings.reducer';
import mapping from './mapping.reducer';

const reducer = combineReducers({
    mappings,
    mapping    
});

export default reducer;
