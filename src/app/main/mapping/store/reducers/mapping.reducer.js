import * as Actions from '../actions';

const initialState = {
    data: null
};

const storeReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_STORE:
        {
            return {
                ...state,
                store: action.payload
            };
        }
        case Actions.GET_MARKET_FIELD:
        {
            return {
                ...state,
                market: action.payload
            };
        }        
        case Actions.GET_OWN_FIELD:
        {
            return {
                ...state,
                own: action.payload
            };
        }        
        case Actions.GET_MAPPING:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_MAPPING:
        {
            return {
                ...state,
                mapData: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default storeReducer;
