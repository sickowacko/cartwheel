import React, {useEffect, useMemo, useState} from 'react';
import {
    Avatar,
    Button,
    Card,
    CardContent,
    OutlinedInput,
    Icon,
    TextField,
    Typography,
    CardActions,
    Divider,
    Select,
    InputLabel,
    FormControl,
    MenuItem,
    LinearProgress
} from '@material-ui/core';
import {makeStyles, useTheme} from '@material-ui/styles';
import {FuseAnimate, FuseAnimateGroup} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import clsx from 'clsx';
import _ from '@lodash';
import {Link} from 'react-router-dom';
import * as Actions from '../courseStore/actions';
import reducer from '../courseStore/reducers';

const useStyles = makeStyles(theme => ({
    header    : {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + theme.palette.primary.main + ' 100%)',
        color     : theme.palette.getContrastText(theme.palette.primary.main)
    },
    headerIcon: {
        position     : 'absolute',
        top          : -64,
        left         : 0,
        opacity      : .04,
        fontSize     : 512,
        width        : 512,
        height       : 512,
        pointerEvents: 'none'
    },
    avatar: {
        // '& .avatar'         : {
            width  : 60,
            height : 60,
            top    : 0,
            padding: 0
        // }

    }
    

}));

function Courses(props)
{
    const dispatch = useDispatch();
    const courses = useSelector(({overallMapApp}) => overallMapApp.courses.data);
    const marketplace = useSelector(({overallMapApp}) => overallMapApp.courses.marketplace);

    const classes = useStyles(props);
    const theme = useTheme();
    const [filteredData, setFilteredData] = useState(null);
    const [searchText, setSearchText] = useState('');
    const [selectedCategory, setSelectedCategory] = useState('all');

    useEffect(() => {
        dispatch(Actions.getMarketplace());
        dispatch(Actions.getStores());
    }, [dispatch]);

    useEffect(() => {
        function getFilteredArray()
        {
            if ( searchText.length === 0 && selectedCategory === "all" )
            {
                return courses;
            }

            return _.filter(courses, item => {
                if ( selectedCategory !== "all" && item.category !== selectedCategory )
                {
                    return false;
                }
                return item.name.toLowerCase().includes(searchText.toLowerCase())
            });
        }

        if ( courses )
        {
            setFilteredData(getFilteredArray());
        }
    }, [courses, searchText, selectedCategory]);

    function handleSelectedCategory(event)
    {
        setSelectedCategory(event.target.value);
    }

    function handleSearchText(event)
    {
        setSearchText(event.target.value);
    }

    // function buttonStatus(course)
    // {
    //     switch ( course.activeStep )
    //     {
    //         case course.totalSteps:
    //             return "COMPLETED";
    //         case 0:
    //             return "START";
    //         default:
    //             return "CONTINUE";
    //     }
    // }

    function cardWithBtn(course){
        let url1 = "";
        let url2 = "";
        //{`/apps/academy/courses/${course.id}/${course.slug}`}

        // console.log("course=",course)

        // switch ( course.activeStep )
        // {
        //     case course.totalSteps:
        //         url =  "/fieldMapping/map1";
        //     case 0:
        //         url =  "/fieldMapping/map1";
        //     default:
        //         url =  '/fieldMapping/'+ course.category + '/' + course.storeId + '/' + course.name;
        // }
        url1 =  '/fieldMapping/createDataSource/'+ course.category + '/' + course.storeId + '/' + course.name;
        url2 = '/fieldMapping/'+ course.category + '/' + course.storeId + '/' + course.name;
        return(
            <CardActions className="justify-center">
                <Button
                    variant="contained" 
                    color="primary"
                    to={url1}
                    component={Link}
                    className="justify-center px-15" 
                    style={{fontSize: "0.9em"}}                     
                >
                    Data Source
                </Button>
                <Button
                    variant="contained" 
                    color="secondary"
                    to={url2}
                    component={Link}
                    className="justify-center px-15"
                    style={{fontSize: "0.9em"}}                    
                >
                  Markeplace Link
                </Button>
            </CardActions>
        )
    }

    return (
        <div className="flex flex-col flex-auto flex-shrink-0 w-full">
            <div
                className={clsx(classes.header, "relative overflow-hidden flex flex-col flex-shrink-0 items-center justify-center text-center p-16 sm:p-24 h-200 sm:h-288")}>

                <FuseAnimate animation="transition.slideUpIn" duration={400} delay={100}>
                    <Typography color="inherit" className="text-24 sm:text-40 font-light">
                        MARKETPLACE & STORE MAPPING
                    </Typography>
                </FuseAnimate>

                <FuseAnimate duration={400} delay={600}>
                    <Typography variant="subtitle1" color="inherit" className="mt-8 sm:mt-16 mx-auto max-w-512">
                            <span className="opacity-75">
                                Step 1 : Upload csv file for marketplace fields<br/>
                                Step 2 : Mapping of store field to maketplace fields
                            </span>
                    </Typography>
                </FuseAnimate>

                <Icon className={classes.headerIcon}>school</Icon>
            </div>

            <div className="flex flex-col flex-1 max-w-2xl w-full mx-auto px-8 sm:px-16 py-24">
                <div className="flex flex-col flex-shrink-0 sm:flex-row items-center justify-between py-24">
                    <TextField
                        label="Search for a store"
                        placeholder="Enter a keyword..."
                        className="flex w-full sm:w-320 mb-16 sm:mb-0 mx-16"
                        value={searchText}
                        inputProps={{
                            'aria-label': 'Search'
                        }}
                        onChange={handleSearchText}
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true
                        }}
                    />
                    <FormControl className="flex w-full sm:w-320 mx-16" variant="outlined">
                        <InputLabel htmlFor="category-label-placeholder">
                            Marketplace
                        </InputLabel>
                        <Select
                            value={selectedCategory}
                            onChange={handleSelectedCategory}
                            input={
                                <OutlinedInput
                                    labelWidth={("category".length * 9)}
                                    name="category"
                                    id="category-label-placeholder"
                                />
                            }
                        >
                            <MenuItem value="all">
                                <em>All</em>
                            </MenuItem>

                            {marketplace.map(category => (
                                <MenuItem value={category.value} key={"opt-"+category.storeId}>{category.label}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
                {useMemo(() => (
                    filteredData && (
                        filteredData.length > 0 ? (
                                <FuseAnimateGroup
                                    enter={{
                                        animation: "transition.slideUpBigIn"
                                    }}
                                    className="flex flex-wrap py-24"
                                >
                                    {filteredData.map((course) => {
                                        const category = marketplace.find(_cat => _cat.value === course.category);
                                        return (
                                            <div className="w-full pb-24 sm:w-1/2 lg:w-1/3 sm:p-16" key={course.storeId}>
                                                <Card elevation={1} className="flex flex-col h-356">
                                                    <div
                                                        className="flex flex-shrink-0 items-center justify-between px-24 h-64"
                                                        style={{
                                                            background: category.color,
                                                            color     : theme.palette.getContrastText(category.color)
                                                        }}
                                                    >
                                                        <Typography className="font-medium truncate" color="inherit">{category.label}</Typography>
                                                        
                                                    </div>
                                                    <CardContent className="flex flex-col flex-auto items-center justify-center">
                                                        <Typography className="text-center text-16 font-400">{course.name}</Typography>
                                                        <Avatar className={classes.avatar} alt={course.name} src={course.avatar}/>
                                                        <Typography className="text-center text-13 font-600 mt-4" color="textSecondary">Updated at: {course.updated}</Typography>
                                                    </CardContent>
                                                    <Divider/>

                                                    {cardWithBtn(course)}                                                    
                                                    
                                                </Card>
                                            </div>
                                        )
                                    })}
                                </FuseAnimateGroup>
                            ) :
                            (
                                <div className="flex flex-1 items-center justify-center">
                                    <Typography color="textSecondary" className="text-24 my-24">
                                        No courses found!
                                    </Typography>
                                </div>
                            )
                    )), [marketplace, filteredData, theme.palette])}
            </div>
        </div>
    );
}

export default withReducer('overallMapApp', reducer)(Courses);
