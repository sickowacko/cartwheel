import React from 'react';
import {Redirect} from 'react-router-dom';

export const MappingConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/fieldMapping/overallMapping',
            component: React.lazy(() => import('./courses/Courses'))
        },
        {
            path     : '/fieldMapping/createDataSource/:marketplace/:storeId/:storeName?',
            component: React.lazy(() => import('./mapping/UploadMarketField'))
        },
        {
            path     : '/fieldMapping/store',
            component: React.lazy(() => import('./mappings/Mappings'))
        },
        {
            path     : '/fieldMapping/:marketplace/:storeId/:storeName?',
            component: React.lazy(() => import('./mapping/Mapping'))
        }
        
        // {
        //     path     : '/fieldMapping/store',
        //     component: () => <Redirect to="/fieldMapping/store"/>
        // }
    ]
};