import * as Actions from '../actions';

const initialState = {
    data      : null,
    marketplace: []
};

const coursesReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_STORES:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.GET_MARKETPLACE:
        {
            return {
                ...state,
                marketplace: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default coursesReducer;
