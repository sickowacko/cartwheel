import axios from 'axios';

export const GET_STORES = 'GET STORES';
export const GET_MARKETPLACE = 'GET MARKETPLACE';

export function getStores()
{
    const request = axios.get('/api/overallMapping/stores');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_STORES,
                payload: response.data
            })
        );
}

export function getMarketplace()
{
    const request = axios.get('/api/overallMapping/marketplaces');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_MARKETPLACE,
                payload: response.data
            })
        );
}
