import React, {useEffect, useState} from 'react';
import {Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {FuseAnimate, FuseExample, FusePageCarded, FusePageSimple, FuseChipSelectNew, FuseUtils, FuseChipSelect} from '@fuse';
import {useForm} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import HomeIcon from '@material-ui/icons/Home';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
// import Link from '@material-ui/core/Link';
import {orange} from '@material-ui/core/colors';
import UploadCSV from "./UploadCSV";

const useStyles = makeStyles(theme => ({
    
}));

function UploadMarketField(props)
{
    const dispatch = useDispatch();
    const product = useSelector(({eCommerceApp}) => eCommerceApp.product);

    const classes = useStyles(props);
    const [tabValue, setTabValue] = useState(0);
    const [headerDetail, setHeader] = useState(0);
    const {form, handleChange, setForm} = useForm(null);
    

    useEffect(() => {
        function updateProductState()
        {
            const params = props.match.params;
            const {productId} = params;
            console.log("param=",params)
            if ( productId === 'new' )
            {
                // dispatch(Actions.newProduct());
                setHeader(params);
            }
            else
            {
                setHeader(params);
                // dispatch(Actions.getProduct(props.match.params));
            }
        }

        updateProductState();
    }, [dispatch, props.match.params]);

    // useEffect(() => {
    //     if (
    //         (product.data && !form) ||
    //         (product.data && form && product.data.id !== form.id)
    //     )
    //     {
    //         setForm(product.data);
    //     }
    // }, [form, product.data, setForm]);

    function handleChangeTab(event, tabValue)
    {
        setTabValue(tabValue);
    }

    function handleChipChange(value, name)
    {
        setForm(_.set({...form}, name, value.map(item => item.value)));
    }

    function setFeaturedImage(id)
    {
        setForm(_.set({...form}, 'featuredImageId', id));
    }

    function handleUploadChange(e)
    {
        const file = e.target.files[0];
        if ( !file )
        {
            return;
        }
        const reader = new FileReader();
        reader.readAsBinaryString(file);

        reader.onload = () => {
            setForm(_.set({...form}, `images`,
                [
                    {
                        'id'  : FuseUtils.generateGUID(),
                        'url' : `data:${file.type};base64,${btoa(reader.result)}`,
                        'type': 'image'
                    },
                    ...form.images
                ]
            ));
        };

        reader.onerror = function () {
            console.log("error on load image");
        };
    }

    function canBeSubmitted()
    {
        return (
            form.name.length > 0 &&
            !_.isEqual(product.data, form)
        );
    }

    function pageHeader(){
        let str = "";
        if(headerDetail) {
            str = headerDetail.marketplace +'<br>' + headerDetail.storeId + "." + headerDetail.storeName ;
            
        } else {
            str = "Imported Fields";
        }
        // str = headerDetail?
        //       headerDetail.marketplace + <br/> + headerDetail.storeId + "." + headerDetail.storeName 
        //       : "Imported Fields";

        return str
    }

    return (
        <FusePageSimple
            header={
                <div className="flex flex-1 items-center justify-between p-24">
                    <div className="flex flex-col">
                        <div className="flex items-center mb-16">
                            {/* <UploadCSV/>                             */}
                            {/* <Icon className="text-18" color="action">home</Icon>
                            <Icon className="text-16" color="action">chevron_right</Icon>
                            <Typography color="textSecondary">Documentation</Typography>
                            <Icon className="text-16" color="action">chevron_right</Icon>
                            <Typography color="textSecondary">3rd Party Components</Typography> */}
                        </div>
                        <Typography variant="h6">{pageHeader()}</Typography>
                    </div>                    
                </div>
            }
            content={
                <div className="p-24 max-w-2xl">
                    
                    
                        
                    

                    <Typography className="mb-16" component={'span'}>
                        <UploadCSV/> 
                        <code>formsy-react</code> is a form input builder and validator for React.
                    </Typography>

                    <Typography className="mb-16" component="p">
                        HOCs are needed for formsy to work. We created for TextField, Select, RadioGroup, Checkbox under @fuse.
                    </Typography>

                    <hr/>

                    <Typography className="text-32 mt-32 mb-8" component="h2">Example Usages</Typography>

                    {/* <FuseExample
                        className="mb-64"
                        component={require('./examples/SimpleFormExample.js').default}
                        raw={require('!raw-loader!./examples/SimpleFormExample.js')}
                    /> */}

                    <Typography className="text-32 mt-32 mb-8" component="h2">Demos</Typography>

                    
                </div>
            }
        />
    )
}

export default withReducer('eCommerceApp', reducer)(UploadMarketField);
