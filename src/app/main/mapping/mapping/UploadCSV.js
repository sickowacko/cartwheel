import React, {useState} from 'react';
import Papaparse from 'papaparse';
import {Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/styles';
import _ from '@lodash';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import {useForm} from '@fuse/hooks';
import {FuseAnimate, FuseExample, FusePageCarded, FusePageSimple, FuseChipSelectNew, FuseUtils, FuseChipSelect} from '@fuse';
import * as utils from '../../../Utils/Utils';
import {blue} from '@material-ui/core/colors';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import FormGroup from '@material-ui/core/FormGroup';


const useStyles = makeStyles(theme => ({
    button: {
      margin: theme.spacing(1),
      backgroundColor: blue[600],
      color: "white",
      '&:hover'              : {
            backgroundColor: blue[400],
            opacity        : 1
        }
    },
    input: {
      display: 'none',
    },
    root: {
        flexGrow: 1,
        justifyContent: 'center',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
  }));

const AntSwitch = withStyles(theme => ({
    root: {
      width: 28,
      height: 16,
      padding: 0,
      display: 'flex',
    },
    switchBase: {
      padding: 2,
      color: theme.palette.grey[500],
      '&$checked': {
        // transform: 'translateX(12px)',
        color: theme.palette.common.white,
        '& + $track': {
          opacity: 1,
          backgroundColor: theme.palette.primary.main,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 10,
      height: 10,
      boxShadow: 'none',
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    checked: {},
  }))(Switch);  

function UploadCSV(props)
{
    const dispatch = useDispatch();
    const upload = useSelector(({uploadCSVApp}) => uploadCSVApp);
    const {form, handleChange, setForm} = useForm({checkedC: true});
    const classes = useStyles();
    
    const [formSubmit, setFormValid] = useState(0);
    const [state, setState] = useState({   
        checkedC: true,
      });

    function handleUploadChange(e)
    {
        const file = e.target.files[0];
        if ( !file )
        {
            return;
        }
        const reader = new FileReader();
        reader.readAsBinaryString(file);

        reader.onload = () => {
            setForm(_.set({...form}, `images`,
                [
                    {
                        'id'  : FuseUtils.generateGUID(),
                        'url' : `data:${file.type};base64,${btoa(reader.result)}`,
                        'type': 'image'
                    },
                    ...form.images
                ]
            ));
        };

        reader.onerror = function () {
            console.log("error on load image");
        };
    }   

    function handleFileChange(e) {
        const file = e.target.files[0];
        if (!file) {
            return;
        } else {
            setForm(_.set({ ...form }, `csv_data`, file));
            setFormValid(true);
        }
        

        // const reader = new FileReader();
        // reader.readAsDataURL(file);
        // reader.onload = () => {
        //     console.log(file.name)
        //     // setForm(_.set({ ...form }, `preview_image_b`, reader.result));
        //     setForm(_.set({ ...form }, `import_csv`, file));
        // };
        // reader.onerror = function () {
        //     console.log("error on load image");
        // };
    }

    function submitForm(form){  
        if(form.csv_data){
            console.log("form.checked=",form.checkedC);
            utils.parseCsvFileToJson(form.csv_data, form.checkedC).then(rslt => {
                console.log("**** rslt =",rslt)
                if(this.validate(rslt.data)) {
                  if (rslt.errors.length === 0) {
                    console.log("****no error")
                    
                  } else {
                    throw rslt.errors
                  }
                } else {
                    console.log("****error")
                  
                }
              }).catch(err => {
               
              });

        }
        

        if(Object.keys(form).length>0){
            let arr = [];
            // console.log("submit run", form)            

            Object.keys(form).map( (ind, index) =>{ 
                    arr.push(form[ind]);
                    if(index== Object.keys(form).length-1) {                        
                        // console.log("final arr= ",arr);
                        dispatch(Actions.saveMapping(arr));
                    } 
            })
        }  
    }

    const handleCheckbox = name => event => {
        console.log("event.target.checked=",event.target.checked)
        // setState({ ...state, [name]: event.target.checked });
        setForm(_.set({ ...form }, [name], event.target.checked));
    };

    return (
        <div> 
            <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={2}>
                <Grid item>
                    <input        
                        className={classes.input}
                        id="text-button-file"
                        multiple
                        type="file"
                        onChange={handleFileChange}
                    />
                    <label htmlFor="text-button-file">
                        <Button component="span" className={classes.button}>
                            <Icon className="mr-4">link</Icon>
                            Upload CSV
                        </Button>
                    </label>                    
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                    <Grid item xs>
                        {/* <Typography gutterBottom variant="subtitle1">
                        Standard license
                        </Typography> */}
                        <Typography component={'div'} variant="body2" gutterBottom>
                            <FormGroup>
                                <Grid component="label" container alignItems="center" spacing={1}>
                                    <Grid item>No Header</Grid>
                                        <Grid item>
                                            <AntSwitch
                                            checked={form.checkedC}                                        
                                            onChange={handleCheckbox('checkedC')}
                                            value="checkedC"
                                            size="medium"
                                            />
                                        </Grid>
                                    <Grid item>With Header</Grid>
                                </Grid>
                            </FormGroup>
                        </Typography>                        
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" style={{ cursor: 'pointer' }}>
                        Clear
                        </Typography>
                    </Grid>
                    </Grid>
                    <Grid item>
                    <Typography variant="subtitle1">
                        <Button
                            className="whitespace-no-wrap"
                            variant="contained"
                            disabled={!formSubmit}
                            onClick={()=>submitForm(form)}
                        >
                            Import CSV
                        </Button>
                    </Typography>
                    </Grid>

                    
                </Grid>
                </Grid>
            </Paper>

                
            </div>

            

      

        </div>
        
    )
}

export default withReducer('uploadCSVApp', reducer)(UploadCSV);