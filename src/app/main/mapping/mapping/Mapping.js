import React, {useEffect, useState} from 'react';
import {Button, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {FuseAnimate, FusePageSimple, FuseChipSelectNew} from '@fuse';
import {useForm} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import _ from '@lodash';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import HomeIcon from '@material-ui/icons/Home';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
// import Link from '@material-ui/core/Link';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      paddingLeft: '5%',
      paddingRight: '10%',
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    contentCard:{
        padding: '5px',
    },
    ownPaper: {
        flexGrow: 1,
        paddingLeft: '1%',
        paddingRight: '10%',
        backgroundColor: 'transparent',
    },
    breadcrumbLink: {
        color: 'white'
    }
}));

function Mapping(props)
{
    const dispatch = useDispatch();
    const product = useSelector(({MapApp}) => MapApp.mapping);

    const classes = useStyles(props);   
    const [marketplace, setMarketplace] = useState(0); 
    const [storeName, setStoreName] = useState(0);
    const [ownFld, setOwnFld] = useState(0);
    const [initRslt, setInitFld] = useState(0);
    const [formSubmit, setFormValid] = useState(0);
    const {form, handleChange, setForm} = useForm(null);

    useEffect(() => {
        function updateProductState()
        {
            const params = props.match.params;
            const {storeId} = params;
            
            if ( storeId === 'new' )
            {
                dispatch(Actions.newStore());
            }
            else
            {
                let param = { storeId: params.storeId}
                setMarketplace(params.marketplace);  
                setStoreName(params.storeName);                
                // dispatch(Actions.getStore(param));
                let marketplace = params.marketplace;                             
                let rslt_marketplace = marketplace.substring(0,1).toLowerCase() + marketplace.substring(1,marketplace.length);
                
                dispatch(Actions.getMarketField(rslt_marketplace));
                dispatch(Actions.getOwnField());
                dispatch(Actions.getMapping());
            }
        }

        updateProductState();        
    }, [dispatch, props.match.params]);

    useEffect(() => {        
        if (
            (product.data && !form) ||
            (product.data && form && product.data.id !== form.id)
        )
        {
            console.log("product.market=",product.market)
            if(product.market) {
                setInitVar(product.market).then(rslt=>{
                    if(rslt){
                        console.log("from promise rslt=",rslt)
                        setForm(rslt);
                    }
                }).catch(err=>console.log("err"))
            }
            
              
        }

        if(form){
            canBeSubmitted();
        }

        // return () => {
        //     setForm(null);
        //      console.log("componentWillUnmount"); }
        
    }, [form, product.data, setForm]);

    useEffect(() => {
        let arr =[];
        if (product.own)
        {
            arr = product.own.map(ind=>{
                    return ({
                                value: ind.name,
                                label: ind.label

                            })
                    
                  })
            setOwnFld(arr); 
        }
    }, [product.own, setOwnFld]);

    // useEffect(() => {
    //     return () => {
    //       console.log("cleaned up");
    //     };
    //   }, []);

    function setInitVar(market){

        return new Promise((resolve, reject)=>{
            let rslt = product.data;
            let arrange_var = [];
            let obj = {};
           
            market.map((ind, index)=>{            
    
                        for(let a in rslt){
                            let rslt_fieldName = Object.keys(rslt[a]);
                            arrange_var[index] = {};
                            if(ind.name.indexOf(rslt_fieldName)>-1 ){
                                                        
                                arrange_var[index] = { [ind.name]: rslt[a][rslt_fieldName] }
                                break;
                            } else{
                                
                                arrange_var[index] = { [ind.name]: {key: "", value: ""} }
                            }
    
                            if(index == market.length-1){
                                console.log("finish setup=",arrange_var)
                                setInitFld(arrange_var);
                                resolve(arrange_var);
                            }
                        }
            });
        })
    }

    function handleChipChangeSingle(value, index, name)
    {    
        let arr =[];        
        if(!Array.isArray(value)){
                arr.push(value);            
        }
        value = arr;

        let fin_val = {};
        fin_val[name] = value.map(item => ({
            value: item.value,
            key: item.label
        })
        )
        
        
        let fin_obj = {};
        fin_obj[name] = fin_val[name][0];        
        
        setForm(_.set({...form}, index, fin_obj));   
          
    }

    function canBeSubmitted()
    {           
        
        if(form!=null && Object.keys(form).length > 0){
            fieldsValid().then(rslt=>{
                if(rslt){
                    console.log("form change submit")
                    setFormValid(true);
                   
                } else{                    
                   
                    setFormValid(false);
                   
                }
            })
            
        }       
    }

    function fieldsValid(){        
        return new Promise((resolve, reject)=>{
            let valid= false;

            Object.keys(form).map((ind)=>{
                
                let keys = Object.keys(form[ind]);
                if(form[ind][keys].value!="" && form[ind][keys].value!=undefined){                                  
                    resolve(true);
                }

                else if(ind == Object.keys(form).length-1 && !valid){                    
                    resolve(valid);
                }
            })

        })  
    }

    function geneOption(option) {
        if (Array.isArray(option)) {           
            let initialOption = option
            let o = [];   
            let default_ = [{ value: "", label: "-Select field-"}] ;     
            
            o = initialOption.map(option =>({
                        value: option.value,
                        label: option.label
                }) 
            )
            // console.log("option end=",o)
            return default_.concat(o);
        }
    }

    function pTabs() {            
        if (marketplace != null) {
            return (
                <Typography variant="h6">Mapping of marketplace {marketplace} to {storeName}</Typography>               
            )
        }
    };

    function generateTblHeader(){
        let head = product.store;
        console.log("head=",head);

        if(head!=null){
            return(
                <React.Fragment>                     
                    <Grid item xs={2}>
                        <Paper className={classes.paper} id="left-label-header">{head[0].name}</Paper>
                    </Grid>
                    <Grid item xs={10}>
                        <Paper className={classes.paper}>own field</Paper>
                    </Grid>                        
                </React.Fragment>
    
            )

        }        
    }

    function geneSelectField(fieldName, fieldLabel, index){
        // console.log("gene fieldName=",form,"-",form?form[index][fieldLabel]:"");
        return (
            <FuseChipSelectNew
                className="mt-8 mb-16 fuseSelectField"                             
                value={
                    form?
                    Object.keys(form).map(item => {  
                        if(fieldName == Object.keys(form[item])){
                            let key = Object.keys(form[item])
                            // console.log("1.item=",form[item])
                            // console.log("2. item=",item)
                            return {
                                value: form[item][key].value,
                                label: form[item][key].key
                            }
                        }
                    }
                    )
                    :
                    ""
                }
                onChange={(value) => handleChipChangeSingle(value, index, fieldName)}            
                placeholder="-Select field-"
                textFieldProps={{
                    // label          : fieldName,
                    InputLabelProps: {
                        shrink: true
                    },
                    variant        : 'outlined'
                }}
                options={geneOption(ownFld)}
                isMulti={false}
            />

        )
        
    }

    function generateTblContent(){
        let fld = product.market;

        if(fld!=null){           

            return(
                fld.map( (ind, index)=>{                    
                    return(
                        <React.Fragment  key={ind.name}>                        
                            <Grid item xs={3}>
                                <div id="left-label"><span>{ind.label}</span></div>
                            </Grid>
                            <Grid item xs={3}>
                                <div className="select-col">{generateInputFld(ind.name, ind.label, index)}</div>
                            </Grid>                        
                        </React.Fragment> 
                    ) 
                })
            )
        }

    }

    function generateInputFld(marketFldName, marketFldLabel, index){                 
        return geneSelectField(marketFldName, marketFldLabel, index);
    }

    function submitForm(form){
        
        if(Object.keys(form).length>0){
            let arr = [];
            // console.log("submit run", form)            

            Object.keys(form).map( (ind, index) =>{ 
                    arr.push(form[ind]);
                    if(index== Object.keys(form).length-1) {                        
                        // console.log("final arr= ",arr);
                        dispatch(Actions.saveMapping(arr));
                    } 
            })
        }  
    }
    
    // console.log("canBeSubmitted()=",canBeSubmitted())      
    return (
          

        <FusePageSimple            
            classes={{
                toolbar: "map-toolbar",
                header : "min-h-52 h-52 sm:h-52 sm:min-h-52"
            }}
            header={
                
                    <div className="flex flex-1 w-full items-center justify-between" id="mapping-header">
                        <Paper elevation={0} className={classes.ownPaper}>
                                <Breadcrumbs aria-label="breadcrumb">
                                <Typography className="normal-case flex items-center" 
                                    component={Link} role="button" to="/fieldMapping/store" color="textSecondary">
                                    <HomeIcon className={classes.icon} />
                                    Back to Stores
                                </Typography>    
                                    {/* <Link color="textPrimary" className={classes.breadcrumbLink} href="/fieldMapping/store">
                                    <HomeIcon className={classes.icon} />
                                    Stores
                                    </Link> */}
                                    <Typography color="textPrimary" className={classes.link}>                            
                                        {storeName ? storeName : 'Store Name'}
                                    </Typography>
                                </Breadcrumbs>
                                
                            </Paper>
                            <br/>
                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Button
                                    className="whitespace-no-wrap"
                                    variant="contained"
                                    disabled={!formSubmit}
                                    onClick={()=>submitForm(form)}
                                >
                                    Save
                                </Button>
                            </FuseAnimate>
                    </div>
            }

            contentToolbar={pTabs()}    

            content= {
                <div className={classes.root}>                    
                     <Grid container spacing={1}>
                        {/* {generateTblHeader()} */}
                    
                        {generateTblContent()}
                    </Grid>
                </div>
            }            
            innerScroll
        />
    )
}

export default withReducer('MapApp', reducer)(Mapping);
