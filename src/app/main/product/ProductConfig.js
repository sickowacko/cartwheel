import React from 'react';
import {Redirect} from 'react-router-dom';

export const ProductConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/product/:id',
            component: React.lazy(() => import('./product/Products'))
        },
        
        {
            path     : '/product_detail/:productId/:productHandle?',
            component: React.lazy(() => import('./product/Product'))
        },
        {
            path     : '/product',
            component: () => <Redirect to="/product/all"/>
        }
    ]
};