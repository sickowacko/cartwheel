import React, { useEffect, useRef } from 'react';
import { Fab, Icon } from '@material-ui/core';
import { FusePageSimple, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import ProductsList from './ProductsList';
import ProductsHeader from './ProductsHeader';
import ProductsSidebarContent from './ProductsSidebarContent';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
    addButton: {
        position: 'absolute',
        right: 12,
        bottom: 12,
        zIndex: 99
    }
});

function Products(props) {
    const dispatch = useDispatch();
    const totalproducts = useSelector(({ eCommerceApp }) => eCommerceApp.products.total);
    const classes = useStyles(props);
    const pageLayout = useRef(null);

    useEffect(() => {
        let param = {
            offset: 0,
            limit: totalproducts,
            filter: [],
            token: localStorage.getItem('jwt_access_token'),
            routeParams: props.match.params
        }
        dispatch(Actions.getDataList(param));
    }, [dispatch, props.match.params]);

    useEffect(() => {
        let param = {
            offset: 0,
            limit: totalproducts,
            filter: [],
            token: localStorage.getItem('jwt_access_token'),
            routeParams: props.match.params
        }
        dispatch(Actions.getDataList(param));
        // console.log("**",props.match.params)
    }, [dispatch, props.match.params]);

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-24 sm:pb-24 h-full",
                    content: "flex flex-col h-full",
                    leftSidebar: "w-256 border-0",
                    header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
                }}
                header={
                    <ProductsHeader pageLayout={pageLayout} />
                }
                content={
                    <ProductsList />
                }
                leftSidebarContent={
                    <ProductsSidebarContent />
                }
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
            {/* <FuseAnimate animation="transition.expandIn" delay={300}>
                <Fab
                    color="primary"
                    aria-label="add"
                    className={classes.addButton}
                    onClick={ev => dispatch(Actions.openNewContactDialog())}
                >
                    <Icon>person_add</Icon>
                </Fab>
            </FuseAnimate>
            <ContactDialog/> */}
        </React.Fragment>
    )
}

export default withReducer('eCommerceApp', reducer)(Products);
