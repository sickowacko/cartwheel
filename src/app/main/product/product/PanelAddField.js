import React, { useEffect, useState } from 'react';
import { Button, IconButton, Icon, TextField, ClickAwayListener, Typography } from '@material-ui/core';
import * as Actions from '../store/actions';
import { useForm } from '@fuse/hooks';
import { useDispatch} from 'react-redux';

function PanelAddField(props) {
    const dispatch = useDispatch();

    const [formOpen, setFormOpen] = useState(false);
    const { form, handleChange, resetForm } = useForm({
        label: '',
        name: '',
        field_type: ''
    });

    useEffect(() => {
        if (!formOpen) {
            resetForm();
        }
    }, [formOpen, resetForm]);

    function handleOpenForm() {
        setFormOpen(true);
    }

    function handleCloseForm() {
        setFormOpen(false);
    }

    function handleSubmit(ev) {
        ev.preventDefault();
        console.log("New Field: ", form)
        props.onFieldAdded(form, props.fieldOwner, props.list);
        handleCloseForm();
    }

    function isFormInvalid() {
        return form.label.length === 0 && form.name.length === 0 && form.field_type.length === 0;
    }

    return (
        <div className="w-full border-t-1">
            {formOpen ? (
                <ClickAwayListener onClickAway={handleCloseForm}>
                    <form className="p-16" onSubmit={handleSubmit}>
                        <Typography gutterBottom>Add New Field</Typography>

                        <TextField
                            className="mb-16"
                            required
                            fullWidth
                            variant="outlined"
                            label="Label"
                            autoFocus
                            name="label"
                            value={form.label}
                            onChange={handleChange}
                        />

                        <TextField
                            className="mb-16"
                            required
                            fullWidth
                            variant="outlined"
                            label="Name"
                            autoFocus
                            name="name"
                            value={form.name}
                            onChange={handleChange}
                        />

                        <TextField
                            className="mb-16"
                            required
                            fullWidth
                            variant="outlined"
                            label="Field Type"
                            autoFocus
                            name="field_type"
                            value={form.field_type}
                            onChange={handleChange}
                        />

                        <div className="flex justify-between items-center">
                            <Button
                                variant="contained"
                                color="secondary"
                                type="submit"
                                disabled={isFormInvalid()}
                            >
                                Add
                            </Button>

                        </div>
                    </form>
                </ClickAwayListener>
            ) : (
                    <Button
                        onClick={handleOpenForm}
                        classes={{
                            root: "normal-case font-600 w-full rounded-none h-48",
                            label: "justify-start"
                        }}
                    >
                        <Icon className="text-20 mr-8">add</Icon>
                        Add field
                </Button>
                )}
        </div>
    );
}

export default PanelAddField;