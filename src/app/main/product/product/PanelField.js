import React from 'react';
import { TextField } from '@material-ui/core';
import clsx from 'clsx';
import moment from 'moment';
import _ from '@lodash';
import * as Actions from '../store/actions';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '@fuse/hooks';

function PanelField(props) {

    const { form, handleChange} = useForm({
        input1: '',
    });

    return (
        <TextField
            className="mt-8 mb-8"
            required
            label={props.form.label}
            type={props.form.field_type}
            id={props.form.name}
            name={props.form.name}
            value={form[props.form.name]}
            onChange={handleChange}
            variant="outlined"
            multiline
            fullWidth
        />
    );
}

export default PanelField;
