import React, { useEffect, useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import { FuseAnimate } from '@fuse';
import * as Actions from '../store/actions';
import { useDispatch, useSelector } from 'react-redux';

import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
}));

const styles = theme => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

function Related(props) {

    const dispatch = useDispatch();
    const totalproducts = useSelector(({ eCommerceApp }) => eCommerceApp.products.total);
    const searchText = useSelector(({ eCommerceApp }) => eCommerceApp.products.searchText2);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [selected, setSelected] = useState(props.rInitialValue);
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    useEffect(() => {
        let param = {
            offset: 0,
            limit: totalproducts,
            filter: [],
            token: localStorage.getItem('jwt_access_token'),
            routeParams: { id: "all"}
        }
        dispatch(Actions.getDataList(param));
    }, [dispatch]);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    function handleCheck(event, sku) {
        const selectedIndex = selected.indexOf(sku);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, sku);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
        // let s = newSelected.push([{'classes': props.rClasses}])
        // console.log(s)
        props.rSelectedValue(newSelected, props.rName, props.rClasses);
    }

    function handleChangePage(event, newPage) {
        setPage(newPage);
    }

    function handleChangeRowsPerPage(event) {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }

    return (
        <div>
            <div>
                <Typography variant="body1">{props.rLabel} <Button className="pl-0 pr-0 mb-8 " color="secondary" onClick={handleClickOpen}><Icon>edit</Icon></Button></Typography>
            </div>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open} maxWidth="none">
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>{props.rLabel} SKU</DialogTitle>
                <DialogContent dividers>
                    {/* <FuseAnimate animation="transition.slideDownIn" delay={300}>
                        <Paper className="flex items-center w-full max-w-512 px-8 py-4 rounded-8" elevation={1}>

                            <Icon className="mr-8" color="action">search</Icon>

                            <Input
                                placeholder="Search"
                                className="flex flex-1"
                                disableUnderline
                                fullWidth
                                value={searchText}
                                inputProps={{
                                    'aria-label': 'Search'
                                }}
                                onChange={ev => dispatch(Actions.setRelatedSearchText(ev))}
                            />
                        </Paper>
                    </FuseAnimate> */}
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow
                            >
                                <TableCell padding="checkbox">
                                    {/* <Checkbox
                                    /> */}
                                </TableCell>
                                <TableCell>SKU</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell align="center">Status</TableCell>
                                <TableCell align="center">Image</TableCell>
                                {/* <TableCell align="center">Classes</TableCell> */}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {props.rData.map(row => {
                                const isSelected = selected.indexOf(row) !== -1;
                                return (
                                    <TableRow
                                        role="checkbox"
                                        hover
                                        tabIndex={-1}
                                        selected={isSelected}
                                        key={row.sku}
                                    >
                                        <TableCell padding="checkbox">
                                            <Checkbox
                                                checked={isSelected}
                                                onClick={event => event.stopPropagation()}
                                                onChange={event => handleCheck(event, row)}
                                            />
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {row.sku}
                                        </TableCell>
                                        <TableCell>{row.name}</TableCell>
                                        <TableCell align="center">{row.status}</TableCell>
                                        <TableCell align="center">{row.thumbnail_image}</TableCell>
                                        {/* <TableCell align="center">{props.rClasses}</TableCell> */}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </DialogContent>
                <DialogActions>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={props.rData.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                            'aria-label': 'previous page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'next page',
                        }}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default withReducer('eCommerceApp', reducer)(Related);