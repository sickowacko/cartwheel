import React, { useEffect, useState } from 'react';
import { Avatar, Checkbox, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import { withRouter } from 'react-router-dom';
import * as Actions from '../store/actions';
import ProductsMultiSelectMenu from './ProductsMultiSelectMenu';

const imageSrc = "http://wowsyncpim.appscentral.net/var/assets/thumbnail/product/";

function ProductsList(props) {
    const dispatch = useDispatch();
    const products = useSelector(({ eCommerceApp }) => eCommerceApp.products.data);
    const searchText = useSelector(({ eCommerceApp }) => eCommerceApp.products.searchText);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(data, searchText) {
            const arr = Object.keys(data).map((id) => data[id]);
            if (searchText.length === 0) {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if (products) {
            setFilteredData(getFilteredArray(products, searchText));
        }
    }, [products, searchText]);


    if (!filteredData) {
        return null;
    }

    if (filteredData.length === 0) {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                    There are no products
                </Typography>
            </div>
        );
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick: (e, handleOriginal) => {
                            if (rowInfo) {
                                // dispatch(Actions.openEditContactDialog(rowInfo.original));
                                props.history.push('/product_detail/' + rowInfo.original.sku + '/' + rowInfo.original.name);
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        Header: () => (
                            <Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                            // onChange={(event) => {
                            //     event.target.checked ? dispatch(Actions.selectAllContacts()) : dispatch(Actions.deSelectAllContacts());
                            // }}
                            // checked={selectedContactIds.length === Object.keys(contacts).length && selectedContactIds.length > 0}
                            // indeterminate={selectedContactIds.length !== Object.keys(contacts).length && selectedContactIds.length > 0}
                            />
                        ),
                        accessor: "",
                        Cell: row => {
                            return (<Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                            // checked={selectedContactIds.includes(row.value.id)}
                            // onChange={() => dispatch(Actions.toggleInSelectedContacts(row.value.id))}
                            />
                            )
                        },
                        className: "justify-center",
                        sortable: false,
                        width: 64
                    },
                    {
                        // Header: () => (
                        //     selectedContactIds.length > 0 && (
                        //         <ProductsMultiSelectMenu />
                        //     )
                        // ),
                        accessor: "avatar",
                        Cell: row => (
                            <Avatar className="mr-8" src="assets/images/ecommerce/no_img.png" />
                        ),
                        className: "justify-center",
                        width: 64,
                        sortable: false
                    },
                    {
                        Header: "SKU",
                        accessor: "sku",
                        filterable: true,
                    },
                    {
                        Header: "Name",
                        accessor: "name",
                        // Cell: row => (
                        //     <div>
                        //         <span>{row.row.sku} </span>
                        //         <span>{row.row.name}</span>
                        //         <span className="class-for-name">{row.row.product.name}</span>
                        //         <span className="class-for-description">{row.row.product.description}</span>
                        //     </div>
                        // ),
                        filterable: true,
                    },
                    {
                        Header: "Status",
                        accessor: "status",
                        filterable: true
                    },
                ]}
                defaultPageSize={10}
                noDataText="No products found"
            />
        </FuseAnimate>
    );
}

export default withRouter(ProductsList);
