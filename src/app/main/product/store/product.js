import Api from "../../../store/api";

export const getProductTabs = (param) => {
    var query = {
        "query": [
            {
                "fn": "getLayout",
                "param": {
                    "class": "Product"
                }
            }
        ]
    };

    return Api.post(query).then(res => {         
        if(res){
          return res;
        } else {          
          return false;
        }
      }).catch(err => {
        console.log(err);
      })
}

export const getDataList = (param) => {  
    var query = {
        "query": [
            {
                "fn": "getList",
                "param": {
                    "class": "Product",
                    "filter": [],
                    "limit": 20,
                    "offset": 0,
                    "orderKey": [],
                    "order": []
                }                
            }
        ]        
    };

    return Api.post(query).then(res => { 
              
        if(res){            
          return res;
        } else {          
          return false;
        }
      }).catch(err => {
        console.log(err);
      })
}

export const getDataDetail = (param) => {

    var temp = localStorage.getItem('jwt_access_token');

    param.token = temp ? temp : false;

    var query = {
        "query": [
            {
                "fn": "getDetails",
                "param": {
                    "sku": param.sku
                }
            }
        ],
        "token": temp,
        "New": true
    };

    return Api.post(query).then(res => {         
        if(res){
            return res.result[0];
        } else {          
          return false;
        }
      }).catch(err => {
        console.log(err);
      })    
}

export const editData = (param) => {    

    var query = {
        "query": [

            {
                "fn": "updateDetails",
                "param": param
                // {
                //     "sku": param.sku,
                //     "name": param.name,
                //     "status": param.status,
                //     "category": param.category,
                //     "description": param.description,
                //     "brand": param.brand,
                //     "base_image": param.base_image,
                //     "thumbnail_image": param.thumbnail_image,
                //     "related": param.related,
                //     "upsell": param.upsell,
                //     "shop_name": param.shop_name,
                //     "laz_name": param.laz_name,
                //     "barcode": param.barcode,
                //     "url_key": param.url_key,
                //     "product_name_long": param.product_name_long,
                //     "testinput": param.testinput,
                //     "testinput2": param.testinput2,
                // }
            }
        ]
    };

    return Api.post(query).then(res => {         
        if(res){
            return res;
        } else {          
          return false;
        }
      }).catch(err => {
        console.log(err);
      })
}

export const deleteData = (param) => {    

    var query = {
        "query": [

            {
                "fn": "deleteProduct",
                "param": {
                    "sku": param.sku
                }
            }
        ]
    };

    return Api.post(query).then(res => {         
        if(res){
            return res;
        } else {          
          return false;
        }
      }).catch(err => {
        console.log(err);
      })
}

export const getAllBrands = (param) => {
   
    var query = {
        "query": [
            {
                "fn": "getBrandList",
                "param": {
                    "offset": 0,
                    "orderKey": ["brand_name"],
                    "order": ["asc"],
                    "filter": []
                }
            }
        ]
    };

    return Api.post(query).then(res => {         
        if(res){
            return res.result[0].brands;
        } else {          
          return false;
        }
      }).catch(err => {
        console.log(err);
      })    
}