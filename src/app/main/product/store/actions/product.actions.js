import * as Product from "../product";
import axios from 'axios';
import { FuseUtils } from '@fuse';
import { showMessage } from 'app/store/actions/fuse';

export const GET_PRODUCT_TABS = 'GET_PRODUCT_TABS';
export const GET_DATA_DETAIL = 'GET_DATA_DETAIL';
export const EDIT_DATA = 'EDIT_DATA';
export const DELETE_DATA = 'DELETE_DATA';
export const GET_BRANDS = 'GET_BRANDS';
export const GET_ALL_DATA = 'GET_ALL_DATA';

export function getProductTabs(params) {

    // const request = axios.get('/api/test-app/product_tabs');

    // return (dispatch) => {
    //     dispatch({
    //         type: "LOADING",
    //         payload: true
    //     })
    //     request.then((response) =>
    //         dispatch({
    //             type: GET_PRODUCT_TABS,
    //             payload: response.data.result[0]
    //         })
    //     );
    // }

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Product.getProductTabs(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                // console.log(data.result[0])
                dispatch({
                    "type": GET_PRODUCT_TABS,
                    "payload": data.result[0]
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

export function newData() {
    const data = {
        id: FuseUtils.generateGUID(),
        sku: '',
        name: '',
        status: '',
        description: '',
        short_description: '',
        brand: '',
        base_image: '',
        base_image_p: '',
        thumbnail_image: '',
        thumbnail_image_p: '',
        relations_related: [],
        relations_upsell: [],
        relations_category: [],
        shop_name: '',
        laz_name: '',
        barcode: [],
        url_key: '',
        product_name_long: '',
        testinput: '',
        testinput2: '',
        preview_image_b: '',
        preview_image_t: '',
        input1: '',
    };

    return {
        type: GET_DATA_DETAIL,
        payload: data
    }
}

export const getDataDetail = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Product.getDataDetail(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                // console.log(data)
                dispatch({
                    "type": GET_DATA_DETAIL,
                    "payload": data
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

export const editData = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Product.editData(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                // dispatch({
                //     "type": EDIT_DATA,
                //     "payload": data
                // });
                dispatch(showMessage({ message: 'Product Saved' }));
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

export const deleteData = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Product.deleteData(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                dispatch({
                    "type": DELETE_DATA,
                    "payload": data
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

export const getAllBrands = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Product.getAllBrands(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                dispatch({
                    "type": GET_BRANDS,
                    "payload": data
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

export const getDataList = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Product.getDataList(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                dispatch({
                    "type": GET_ALL_DATA,
                    "payload": { data: data.result[0].products, total: data.result[0].total }
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}