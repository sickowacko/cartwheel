import * as Product from "../product";
import axios from 'axios';

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const SET_SEARCH_TEXT = 'SET_SEARCH_TEXT';
export const SET_RELATED_SEARCH_TEXT = 'SET_RELATED_SEARCH_TEXT';
export const GET_ALL_DATA = 'GET_ALL_DATA'

export function getProducts() {
    const request = axios.get('/api/e-commerce-app/products');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type: GET_PRODUCTS,
                payload: response.data
            })
        );
}

export function setSearchText(event) {
    return {
        type: SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function setRelatedSearchText(event) {
    return {
        type: SET_RELATED_SEARCH_TEXT,
        searchText2: event.target.value
    }
}

export const getDataList = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });

        return Product.getDataList(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data && params.routeParams.id === "all") {
                console.log (params.routeParams.id)
                dispatch({
                    "type": GET_ALL_DATA,
                    "payload": { data: data.result[0].products, total: data.result[0].total }
                });
                succ = true;
            } else if (data && params.routeParams.id === "A") {
                console.log (params.routeParams.id)
                dispatch({
                    "type": GET_ALL_DATA,
                    "payload": { data: [], total: "" }
                });
                succ = true;
            } else if (data && params.routeParams.id === "B") {
                console.log (params.routeParams.id)
                dispatch({
                    "type": GET_ALL_DATA,
                    "payload": { data: [], total: "" }
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

