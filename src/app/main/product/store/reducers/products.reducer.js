import * as Actions from '../actions';

const initialState = {
    data: [],
    searchText: '',
    searchText2: '',
    total: [],
};

const productsReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.SET_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText: action.searchText
                };
            }
        case Actions.SET_RELATED_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText2: action.searchText2
                };
            }
        case Actions.GET_ALL_DATA:
            {
                return {
                    ...state,
                    data: action.payload.data,
                    total: action.payload.total
                };
            }
        default:
            {
                return state;
            }
    }
};

export default productsReducer;
