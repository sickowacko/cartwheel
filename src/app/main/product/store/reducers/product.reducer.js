import * as Actions from '../actions';

const initialState = {
    data: null,
    brands: null,
    tabs: null,
    data2: [],
    total: [],
};

const productReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_PRODUCT_TABS:
            {
                return {
                    ...state,
                    tabs: action.payload
                };
            }
        case Actions.GET_DATA_DETAIL:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.EDIT_DATA:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.DELETE_DATA:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.GET_BRANDS:
            {
                return {
                    ...state,
                    brands: action.payload
                };
            }
        case Actions.GET_ALL_DATA:
            {
                return {
                    ...state,
                    data2: action.payload.data,
                    total: action.payload.total
                };
            }
        default:
            {
                return state;
            }
    }
};

export default productReducer;
