import React, { useEffect, useState } from 'react';
import { Divider, Button, Tab, Tabs, TextField, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, ExpansionPanelActions, Icon, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { orange } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/styles';
import { FuseAnimate, FusePageCarded, FuseChipSelect } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import PanelField from './PanelField';
import PanelAddField from './PanelAddField';
import Related from './Related';

const imageSrc = "http://wowsyncpim.appscentral.net/var/assets/thumbnail/product/";

const useStyles = makeStyles(theme => ({
    paper: {
        height: 200,
        width: '100%',
        marginBottom: theme.spacing(1),
        overflowX: 'auto',
    },
    paper2: {
        width: '100%',
        marginBottom: theme.spacing(1),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    textField: {
        marginLeft: 0,
        marginRight: 0
    },
    menu: {
        width: 200,
    },
    productImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    productImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    productImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $productImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $productImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $productImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));

function Marketplace(props) {
    const dispatch = useDispatch();
    const product = useSelector(({ marketplaceApp }) => marketplaceApp.marketplace);

    const classes = useStyles(props);
    const [tabValue, setTabValue] = useState(0);
    const [laz, setLazada] = useState([]);
    const { form, handleChange, setForm } = useForm(null);

    const params = props.match.params;
    const { category } = params.category;
    // const { productName } = params.productHandle;

    useEffect(() => {
        function updateProductState() {

            if (category === 'new') {
                console.log("run sth new")
                dispatch(Actions.getFormat()); 
                // dispatch(Actions.newData());            
                
            }
            else {
                console.log("run sth ")
                dispatch(Actions.getFormat()); 
                //dispatch(Actions.getProduct(props.match.params));
                // dispatch(Actions.newData());
            }
        }
        updateProductState();
    }, [dispatch, props.match.params]);

    useEffect(() => {
       
        if (product.data
            // (product.data && !form) 
        ) {
            console.log("set form=",form)
            setForm(product.data);
        }
    }, [form, product.data, setForm]);

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    //Tabs
    function tabs() {
        let tabs = product.data
        if (tabs != null) {
            console.log("product =",product.data)
            return (
                tabs.map(t =>                    
                    <Tab className="h-64 normal-case font-bold" key={t.label} label={t.label} /> 
                )
            )
        }
    };

    //Content components 
    function content(form) {
        let tabs = product.data
        var cont = []
        if (tabs != null) {
            for (var i = 0; i < tabs.length; i++) {
                let t = tabs[i].childs
                cont.push(
                    t.map(f =>
                        <div>
                            {tabValue === i &&
                                (
                                    <div key={i} className="pt-24 pr-24 pl-24 max-w-2xl">
                                        {
                                           generateField(form, f)
                                        }
                                    </div>
                                )}
                        </div>)
                )
            }
            return cont
        }

    };

    //Field Types
    function accordion(form, child) {
        let accordion = child
        var acc = []
        for (var i = 0; i < accordion.length; i++) {
            let panel = accordion[i].childs
            acc.push(
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="font-bold">{accordion[i].label}</Typography>
                    </ExpansionPanelSummary>
                    <Divider className="mb-8" />
                    {panel.map(p =>
                        <ExpansionPanelDetails className="block pb-8">
                            {
                                fields(form, p)
                            }
                        </ExpansionPanelDetails>
                    )}
                </ExpansionPanel>
            )
        }
        return acc
    };

    function generateField(form, p){      
        
            if(p.field_type === 'select') {
                return <TextField
                            required
                            key={p.name}
                            id={p.name}
                            select
                            fullWidth
                            label={p.label}
                            className={classes.textField}
                            value={form[p.name]}
                            onChange={handleChipChange(p.name)}
                            SelectProps={{
                                native: true,
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                            margin="normal"
                            variant="outlined"
                        >   {option(p.name, p.options)}
                        </TextField>
            } else if(p.field_type === 'input') {
                return <TextField
                            className="mt-8 mb-16"
                            error={form.name === ''}
                            required
                            label={p.label}
                            autoFocus
                            key={p.name}
                            id={p.name}
                            name={p.name}
                            value={form[p.name]}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
            }           
            

        

    }

    function fields(form, p) {
        var fieldType = []
        fieldType.push(            
                p.field_type === 'select'
                        ?
                        <TextField
                            required
                            id={p.name}
                            select
                            fullWidth
                            label={p.label}
                            className={classes.textField}
                            value={form[p.name]}
                            onChange={handleChipChange(p.name)}
                            SelectProps={{
                                native: true,
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                            margin="normal"
                            variant="outlined"
                        >   {option(p.name, p.options)}
                        </TextField>
                        :
                        p.field_type === 'manyToManyObjectRelation' && (p.name === "relations_related" || p.name === "relations_upsell")
                            ?
                            <div>
                                <Related
                                    rName={p.name}
                                    rLabel={p.label}
                                    rData={product.data2}
                                    rSelectedValue={handleSelectedSku}
                                    rInitialValue={form[p.name]}
                                    rClasses={p.classes[0].classes}
                                />
                                {form[p.name] == ""
                                    ?
                                    <Paper className={classes.paper2}>
                                        <Table className={classes.table}>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell>SKU</TableCell>
                                                    <TableCell>Name</TableCell>
                                                    <TableCell align="center">Status</TableCell>
                                                    <TableCell align="center">Image</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                <TableRow>
                                                    <Typography className="mb-20 mt-20 ml-24 print:mb-10" variant="body1">No data available.</Typography>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </Paper>
                                    :
                                    form[p.name].length > 3
                                        ?
                                        <Paper className={classes.paper}>
                                            <Table className={classes.table}>
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell>SKU</TableCell>
                                                        <TableCell>Name</TableCell>
                                                        <TableCell align="center">Status</TableCell>
                                                        <TableCell align="center">Image</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {form[p.name].map(row => (
                                                        <TableRow
                                                            key={row.sku}
                                                        >
                                                            <TableCell component="th" scope="row">
                                                                {row.sku}
                                                            </TableCell>
                                                            <TableCell>{row.name}</TableCell>
                                                            <TableCell align="center">{row.status}</TableCell>
                                                            <TableCell align="center">{row.thumbnail_image}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </Paper>
                                        :
                                        <Paper className={classes.paper2}>
                                            <Table className={classes.table}>
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell>SKU</TableCell>
                                                        <TableCell>Name</TableCell>
                                                        <TableCell align="center">Status</TableCell>
                                                        <TableCell align="center">Image</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {form[p.name].map(row => (
                                                        <TableRow
                                                            key={row.sku}
                                                        >
                                                            <TableCell component="th" scope="row">{row.sku}</TableCell>
                                                            <TableCell>{row.name}</TableCell>
                                                            <TableCell align="center">{row.status}</TableCell>
                                                            <TableCell align="center">{row.thumbnail_image}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </Paper>
                                }
                            </div>
                            :
                            p.field_type === "manyToManyObjectRelation"
                                ?
                                <FuseChipSelect
                                    className="mt-8 mb-8"
                                    value={form[p.name] === "" || form[p.name] == null
                                        ?
                                        null
                                        :
                                        form[p.name].map(item => ({
                                            value: item,
                                            label: item.sku + "-" + item.name
                                        }))}
                                    name={params.name}
                                    onChange={(value) => handleManyChipChange(value, p.name)}
                                    placeholder={p.placeholder}
                                    textFieldProps={{
                                        label: p.label,
                                        InputLabelProps: {
                                            shrink: true
                                        },
                                        variant: 'outlined',
                                    }}
                                    options={option(p.name, p.classes)}
                                    isMulti
                                />
                                :
                                p.field_type === "manyToOneRelation"
                                    ?
                                    <TextField
                                        required
                                        id={p.name}
                                        select
                                        fullWidth
                                        label={p.label}
                                        className={classes.textField}
                                        value={form[p.name]}
                                        onChange={handleChipChange(p.name)}
                                        SelectProps={{
                                            native: true,
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="normal"
                                        variant="outlined"
                                    >   {option(p.name, p.classes)}
                                    </TextField>
                                    :
                                    p.type === "fieldset" || p.type === "fieldcontainer"
                                        ?
                                        <div>
                                            <Typography className="mb-8 print:mb-12" variant="body1">{p.label}</Typography>
                                            {
                                                p.childs.map(c =>
                                                    <TextField
                                                        className="mt-8 mb-8"
                                                        required
                                                        label={c.label}
                                                        type={c.field_type}
                                                        id={c.name}
                                                        name={c.name}
                                                        value={form[c.name]}
                                                        onChange={handleChange}
                                                        variant="outlined"
                                                        multiline
                                                        fullWidth
                                                    />
                                                )
                                            }
                                        </div>
                                        :
                                        p.type === "accordion"
                                            ?
                                            <div>
                                                {panel(form, p.childs)}
                                            </div>
                                            :
                                            p.field_type === "fieldcollections"
                                                ?
                                                <div>
                                                    {pfieldCollection(form, p)}
                                                </div>
                                                :
                                                p.field_type === 'input' || p.field_type === 'wysiwyg'
                                                    ?
                                                    <TextField
                                                        className="mt-8 mb-8"
                                                        required
                                                        label={p.label}
                                                        type={p.field_type}
                                                        id={p.name}
                                                        name={p.name}
                                                        value={form[p.name]}
                                                        onChange={handleChange}
                                                        variant="outlined"
                                                        multiline
                                                        fullWidth
                                                    />
                                                    :
                                                    null
        )
        return fieldType
    }    

    const handleChipChange = name => event => {
        setForm(_.set({ ...form }, [name], event.target.value));
    };

    function handleSelectedSku(checked, name) {
        console.log("Selected Sku: ", checked)
        if (name === "relations_related") {
            setForm(_.set({ ...form }, `relations_related`, checked));
        } else if (name === "relations_upsell") {
            setForm(_.set({ ...form }, `relations_upsell`, checked));
        }
    };

    function handleManyChipChange(value, name) {
        if (value != null) {
            setForm(_.set({ ...form }, name, value.map(item => item.value)));
        }
    }

    function panel(form, child) {
        let accordion = child
        var acc = []
        for (var i = 0; i < accordion.length; i++) {
            let panel = accordion[i].childs
            acc.push(
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography>{accordion[i].label}</Typography>
                    </ExpansionPanelSummary>
                    {panel.map(s =>
                        <ExpansionPanelDetails>
                            {
                                setForm.field_type === "table"
                                    ?
                                    barcode(form, s)
                                    :
                                    <TextField
                                        className="mt-8 mb-8"
                                        required
                                        label={s.label}
                                        type={s.field_type}
                                        id={s.name}
                                        name={s.name}
                                        value={form[s.name]}
                                        onChange={handleChange}
                                        variant="outlined"
                                        multiline
                                        fullWidth
                                    />
                            }
                        </ExpansionPanelDetails>
                    )}
                </ExpansionPanel>
            )
        }
        return acc
    };

    function barcode(form, s) {
        var barc = []
        let v = ""
        if (typeof form[s.name] == "string") {
            v = form[s.name]
        } else {
            if (form[s.name] != undefined) {
                v = form[s.name].join(',')
            }
        }
        barc.push(
            <TextField
                className="mt-8 mb-8"
                required
                label={s.label}
                type={s.field_type}
                id={s.name}
                name={s.name}
                value={v}
                onChange={handleChange}
                variant="outlined"
                multiline
                fullWidth
            />
        )
        return barc
    }

    function pfieldCollection(form, fieldcollection) {
        let fields = fieldcollection

        // handleLazadaChanges(a)
        // console.log(laz)

        return (
            <div>
                <Typography className="mt-16 mb-8 print:mb-12" variant="body1">{fields.label}</Typography>
                {fields.options.lazada != ""
                    ?
                    fields.options.lazada.map(f =>
                        <ExpansionPanel>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography gutterBottom>{f.label}</Typography>
                            </ExpansionPanelSummary>
                            {fields.options.lazada[0].childs.map(l =>
                                <ExpansionPanelDetails>
                                    <TextField
                                        className="mt-8 mb-8"
                                        required
                                        label={l.label}
                                        type={l.field_type}
                                        id={l.name}
                                        name={l.name}
                                        value={form[l.name]}
                                        onChange={handleChange}
                                        variant="outlined"
                                        multiline
                                        fullWidth
                                    />
                                </ExpansionPanelDetails>
                            )}
                            <ExpansionPanelActions>
                                <PanelAddField
                                    onFieldAdded={addField}
                                    fieldOwner={fields.options.lazada[0].childs}
                                    list={form}
                                />
                            </ExpansionPanelActions>
                        </ExpansionPanel>
                    )

                    :
                    null
                }
                {fields.options.shopee != ""
                    ?
                    fields.options.shopee.map(f =>
                        <ExpansionPanel>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography gutterBottom>{f.label}</Typography>
                            </ExpansionPanelSummary>
                            {fields.options.shopee[0].childs.map(l =>
                                <ExpansionPanelDetails>
                                    <PanelField
                                        form={l}
                                    />
                                </ExpansionPanelDetails>
                            )}
                            <ExpansionPanelActions>
                                <PanelAddField
                                    onFieldAdded={addField}
                                    fieldOwner={fields.options.shopee[0].childs}
                                    list={form}
                                />
                            </ExpansionPanelActions>
                        </ExpansionPanel>
                    )
                    :
                    null
                }
            </div>
        )
    };

    function addField(f, o, l) {
        o.push({ "field_type": f.field_type, "label": f.label, "name": f.name, "not_editable": false, "visible_in_grid": false });
        console.log(o)
        // handleLazadaChanges(o)
    }

    //Options
    function option(name, classOption) {
        if (classOption != undefined && name != "status") {
            // console.log("Classes: ", classOption[0].classes)
        }
        if (name === "status") {
            // console.log("statusOptions: ",classOption)
            let initialStatus = classOption
            let status = []
            if (initialStatus != null) {
                status.push(
                    <option key='' value=''>
                    </option>
                )
                status.push(
                    initialStatus.map(option =>
                        <option key={option.key} value={option.value}>
                            {option.key}
                        </option>
                    )
                )
                return status;
            } else {
                return status;
            }
        }
        else if (classOption[0].classes === "Brand") {
            let initialBrand = product.brands
            let brand = []
            if (initialBrand != null) {
                brand.push(
                    initialBrand.map(option =>
                        <option key={option.brand_name} value={option.brand_name}>
                            {option.brand_code} - {option.brand_name}
                        </option>
                    )
                )
                return brand;
            } else {
                return brand;
            }
        } else if (classOption[0].classes === "Product") {
            let initialProduct = product.data2
            let iProduct = []
            if (initialProduct != null) {
                iProduct = initialProduct.map(item => ({
                    value: item,
                    label: item.sku + "-" + item.name
                }))
                return iProduct;
            } else {
                return iProduct;
            }
        } else if (classOption[0].classes === "ProductCategory") {
            let initialProduct = product.data2
            let iProduct = []
            if (initialProduct != null) {
                iProduct = initialProduct.map(item => ({
                    value: item,
                    label: item.sku + "-" + item.name
                }))
                return iProduct;
            } else {
                return iProduct;
            }
        }
    }

    //Submit validation
    function canBeSubmitted() {
        return (
            form.sku != "" &&
            form.name != "" &&
            !_.isEqual(product.data, form)
        );
    }

    //Save all. Barcode (String -> array). 
    function submit(form) {
        let data = form
        if (typeof data.barcode == "string") {
            let str = data.barcode
            data.barcode = str.split(',')
        }
        if (data.base_image_p != '') {
            data.base_image = data.base_image_p.name
        }
        if (data.thumbnail_image_p != '') {
            data.thumbnail_image = data.thumbnail_image_p.name
        }
        data.base_image_p = ''
        data.thumbnail_image_p = ''
        data.preview_image_b = ''
        data.preview_image_t = ''
        console.log(data)
        // dispatch(Actions.editData(data)).then(res => {
        //     if (res) {
        //         props.history.push('/product/all');
        //     }
        // })
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">
                        <div className="flex flex-col items-start max-w-full  text-white">
                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/marketplace/all" color="inherit">
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    Marketplace
                                </Typography>
                            </FuseAnimate>
                            <div className="flex items-center max-w-full">
                                {/* <FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.thumbnail_image.length > 0 ? (
                                        <img className="w-32 sm:w-48 mr-8 sm:mr-16 rounded" src={imageSrc + form.thumbnail_image} alt={form.name} />
                                    ) : (
                                            <img className="w-32 sm:w-48 mr-8 sm:mr-16 rounded" src="assets/images/ecommerce/no_img.png" alt={form.name} />
                                        )}
                                </FuseAnimate> */}
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'New Marketplace'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Marketplace Detail</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap"
                                variant="contained"
                                disabled={!canBeSubmitted()}
                                onClick={() => submit(form)}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="secondary"
                    textColor="secondary"
                    variant="scrollable"
                    scrollButtons="auto"
                    classes={{ root: "w-full h-64" }}
                >
                    {tabs()}
                </Tabs>
            }
            content=
            {
                form && (
                    <div className="pb-8">
                        {content(form)}
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('marketplaceApp', reducer)(Marketplace);
