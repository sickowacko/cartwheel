import React, {useEffect, useMemo, useState} from 'react';
import {
    // Avatar,
    Button,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    IconButton,
    Collapse,
    OutlinedInput,
    Icon,
    TextField,
    Typography,   
    Divider,
    Select,
    InputLabel,
    FormControl,
    MenuItem,
    LinearProgress
} from '@material-ui/core';
import {makeStyles, useTheme} from '@material-ui/styles';
import {FuseAnimate, FuseAnimateGroup} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import clsx from 'clsx';
import _ from '@lodash';
import {Link} from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';


import Avatar from '@material-ui/core/Avatar';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AddIcon from '@material-ui/icons/Add';

import {fade} from '@material-ui/core/styles/colorManipulator';

const useStyles = makeStyles(theme => ({
    header    : {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + theme.palette.primary.main + ' 100%)',
        color     : theme.palette.getContrastText(theme.palette.primary.main)
    },
    headerIcon: {
        position     : 'absolute',
        top          : -64,
        left         : 0,
        opacity      : .04,
        fontSize     : 512,
        width        : 512,
        height       : 512,
        pointerEvents: 'none'
    },
    board   : {
        cursor                  : 'pointer',
        boxShadow               : theme.shadows[0],
        transitionProperty      : 'box-shadow border-color',
        transitionDuration      : theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        background              : theme.palette.primary.dark,
        color                   : theme.palette.getContrastText(theme.palette.primary.dark),
        '&:hover'               : {
            boxShadow: theme.shadows[6]
        }
    },
    newBoard: {
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: fade(theme.palette.getContrastText(theme.palette.primary.main), 0.6),
        '&:hover'  : {
            borderColor: fade(theme.palette.getContrastText(theme.palette.primary.main), 0.8)
        }
    },    
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    avatar: {
        backgroundColor: 'white',
    },
    imgLogo: {
        width: '70%',
        height: 'auto'
    }
    

}));

function Marketplaces(props)
{
    const dispatch = useDispatch();
    const marketplace = useSelector(({marketplaceApp}) => marketplaceApp.marketplaces.data);
    const categories = useSelector(({marketplaceApp}) => marketplaceApp.marketplaces.category);

    const classes = useStyles(props);
    const theme = useTheme();
    const [filteredData, setFilteredData] = useState(null);
    const [searchText, setSearchText] = useState('');
    const [selectedCategory, setSelectedCategory] = useState('all');

    useEffect(() => {
        dispatch(Actions.getMarketplace());
        dispatch(Actions.getCategory());
    }, [dispatch]);

    useEffect(() => {
        function getFilteredArray()
        {   
            // console.log("marketplaces=",marketplace)
            if ( searchText.length === 0 && selectedCategory === "all" )
            {
                return marketplace;
            }

            return _.filter(marketplace, item => {
                if ( selectedCategory !== "all" && item.category !== selectedCategory )
                {
                    return false;
                }
                return item.name.toLowerCase().includes(searchText.toLowerCase())
            });
        }

        if ( marketplace )
        {
            setFilteredData(getFilteredArray());
        }
    }, [marketplace, searchText, selectedCategory]);

    function handleSelectedCategory(event)
    {
        setSelectedCategory(event.target.value);
    }

    function handleSearchText(event)
    {
        setSearchText(event.target.value);
    }

    // function buttonStatus(course)
    // {
    //     switch ( course.activeStep )
    //     {
    //         case course.totalSteps:
    //             return "COMPLETED";
    //         case 0:
    //             return "START";
    //         default:
    //             return "CONTINUE";
    //     }
    // }

    function cardWithBtn(course){
        let url1 = "";
        let url2 = "";
        //{`/apps/academy/courses/${course.id}/${course.slug}`}

        // console.log("course=",course)

        // switch ( course.activeStep )
        // {
        //     case course.totalSteps:
        //         url =  "/fieldMapping/map1";
        //     case 0:
        //         url =  "/fieldMapping/map1";
        //     default:
        //         url =  '/fieldMapping/'+ course.category + '/' + course.id + '/' + course.name;
        // }
        url1 =  '/marketplace/'+ course.category + '/' + course.id + '/' + course.name;
        url2 = '/fieldMapping/'+ course.category + '/' + course.id + '/' + course.name;
        return(
            <CardActions className="justify-center">
                <Button
                    to={url1}
                    component={Link}
                    className="justify-start px-32"
                    color="secondary"
                >
                    Integrate
                </Button>                
            </CardActions>
        )
    }

    return (
        <div className="flex flex-col flex-auto flex-shrink-0 w-full">
            <div
                className={clsx(classes.header, "relative overflow-hidden flex flex-col flex-shrink-0 items-center justify-center text-center p-16 sm:p-24 h-200 sm:h-288")}>

                <FuseAnimate animation="transition.slideUpIn" duration={400} delay={100}>
                    <Typography color="inherit" className="text-24 sm:text-40 font-light">
                        MARKETPLACE & STORE MAPPING
                    </Typography>
                </FuseAnimate>

                <FuseAnimate duration={400} delay={600}>
                    <Typography variant="subtitle1" color="inherit" className="mt-8 sm:mt-16 mx-auto max-w-512">
                            <span className="opacity-75">
                                Step 1 : Upload csv file for marketplace fields<br/>
                                Step 2 : Mapping of store field to maketplace fields
                            </span>
                    </Typography>
                </FuseAnimate>

                <Icon className={classes.headerIcon}>school</Icon>
            </div>

            <div className="flex flex-col flex-1 max-w-2xl w-full mx-auto px-8 sm:px-16 py-24">
                <div className="flex flex-col flex-shrink-0 sm:flex-row items-center justify-between py-24">
                    <TextField
                        label="Search for a marketplace"
                        placeholder="Enter a keyword..."
                        className="flex w-full sm:w-320 mb-16 sm:mb-0 mx-16"
                        value={searchText}
                        inputProps={{
                            'aria-label': 'Search'
                        }}
                        onChange={handleSearchText}
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true
                        }}
                    />
                    <FormControl className="flex w-full sm:w-320 mx-16" variant="outlined">
                        <InputLabel htmlFor="category-label-placeholder">
                            Categories
                        </InputLabel>
                        <Select
                            value={selectedCategory}
                            onChange={handleSelectedCategory}
                            input={
                                <OutlinedInput
                                    labelWidth={("category".length * 9)}
                                    name="category"
                                    id="category-label-placeholder"
                                />
                            }
                        >
                            <MenuItem value="all">
                                <em>All</em>
                            </MenuItem>

                            {categories.map(category => (
                                <MenuItem value={category.value} key={"opt-"+category.id}>{category.label}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                   
                    
                    {/* <Button component={Link} to={"/marketplace/new"}
                            className="whitespace-no-wrap" 
                            variant="contained">
                                <AddIcon />
                        <span>&nbsp;Add New Marketplace</span>                        
                    </Button>        */}
                    
                </div>
                {useMemo(() => (
                    filteredData && (
                        filteredData.length > 0 ? (
                                <FuseAnimateGroup
                                    enter={{
                                        animation: "transition.slideUpBigIn"
                                    }}
                                    className="flex flex-wrap py-24"
                                >
                                    {filteredData.map((course) => {
                                        const category = categories.find(_cat => _cat.value === course.category);  
                                        console.log("course=",course)                                      
                                        return (
                                            <div className="w-full pb-24 sm:w-1/2 lg:w-1/4 sm:p-16" key={course.id}>
                                                <Card elevation={1} className="flex flex-col h-256">                                                   
                                                    <CardHeader
                                                        style={{
                                                            background: category.color,
                                                            color     : theme.palette.getContrastText(category.color)
                                                        }}
                                                        avatar={
                                                        <Avatar aria-label="recipe" 
                                                            className={classes.avatar}
                                                        >
                                                            <span className={"flag-icon flag-icon-"+course.category}></span>
                                                        </Avatar>
                                                        }
                                                        action={
                                                        <IconButton aria-label="settings">
                                                            <MoreVertIcon />
                                                        </IconButton>
                                                        }
                                                        title={category.label}
                                                        subheader="version 1.0.0"
                                                    />
                                                    {/* <CardMedia
                                                        className={classes.media}
                                                        image="/assets/img/Lazada-Malaysia-Logo.jpg"
                                                        title="Paella dish"
                                                    /> */}
                                                        
                                                    <CardContent className="flex flex-col flex-auto items-center justify-center">
                                                        <img src="/assets/img/Lazada-Malaysia-Logo.jpg" 
                                                            className={classes.imgLogo}
                                                            alt={course.name}/>
                                                        
                                                    </CardContent>
                                                    
                                                    <Divider/>

                                                    {cardWithBtn(course)}    

                                                    <LinearProgress
                                                        className="w-full"
                                                        variant="determinate"
                                                        value={course.activeStep * 100 / course.totalSteps}
                                                        color="secondary"
                                                    />                                                
                                                    
                                                </Card>
                                            </div>
                                        )
                                    })}
                                </FuseAnimateGroup>
                            ) :
                            (
                                <div className="flex flex-1 items-center justify-center">
                                    <Typography color="textSecondary" className="text-24 my-24">
                                        No courses found!
                                    </Typography>
                                </div>
                            )
                    )), [marketplace, filteredData, theme.palette])}
            </div>
        </div>
    );
}

export default withReducer('marketplaceApp', reducer)(Marketplaces);
