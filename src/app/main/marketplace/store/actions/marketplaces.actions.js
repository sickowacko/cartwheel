import axios from 'axios';

export const GET_CATEGORY = 'GET CATEGORY';
export const GET_MARKETPLACE = 'GET MARKETPLACE';

export function getCategory()
{
    const request = axios.get('/api/marketplaces/category');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_CATEGORY,
                payload: response.data
            })
        );
}

export function getMarketplace()
{
    const request = axios.get('/api/marketplaces/marketplace');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_MARKETPLACE,
                payload: response.data
            })
        );
}
