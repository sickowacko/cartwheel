import {combineReducers} from 'redux';
import marketplace from './marketplace.reducer';
import marketplaces from './marketplaces.reducer';

const reducer = combineReducers({
    marketplace,
    marketplaces    
});

export default reducer;
