import * as Actions from '../actions';

const initialState = {
    data: null,
    // format: null    
};

const marketplaceReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_FORM:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }

        case Actions.SAVE_FORM:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }    
        case Actions.GET_DATA_DETAIL:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.EDIT_DATA:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.DELETE_DATA:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }        
        // case Actions.GET_ALL_DATA:
        //     {
        //         return {
        //             ...state,
        //             data2: action.payload.data,
        //             total: action.payload.total
        //         };
        //     }
        default:
            {
                return state;
            }
    }
};

export default marketplaceReducer;
