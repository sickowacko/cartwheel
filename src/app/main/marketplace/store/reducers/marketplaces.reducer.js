import * as Actions from '../actions';

const initialState = {
    data      : null,
    category: []
};

const marketplacesReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_MARKETPLACE:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.GET_CATEGORY:
        {
            return {
                ...state,
                category: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default marketplacesReducer;
