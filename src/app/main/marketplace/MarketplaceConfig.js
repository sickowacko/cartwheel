import React from 'react';
import {Redirect} from 'react-router-dom';

export const MarketplaceConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/marketplace/all',
            component: React.lazy(() => import('./marketplaces/Marketplaces'))
        },        
        {
            path     : '/marketplace/:category/:id/:name?',
            component: React.lazy(() => import('./marketplace/Marketplace'))
        },
        {
            path     : '/marketplace/:category',
            component: React.lazy(() => import('./marketplace/Marketplace'))
        },
        // {
        //     path     : '/marketplace/table/:id',
        //     component: React.lazy(() => import('./contacts/ContactsApp'))
        // },        
        // {
        //     path     : '/marketplace/table',
        //     component: () => <Redirect to="/marketplace/table/all"/>
        // }
    ]
};