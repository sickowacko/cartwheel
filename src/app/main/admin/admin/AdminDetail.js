import React, { useEffect, useState } from 'react';
import { Button, Tab, Tabs, TextField, Icon, Typography, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import { FuseAnimate, FusePageCarded, FuseChipSelect } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link } from 'react-router-dom';
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import * as Action from 'app/store/actions';
import reducer from '../store/reducers';

const suggestions = [
    {
        value: "ROLE_ADMIN",
        label: "Admin"
    },
    {
        value: "ROLE_STORE_SUPERVISOR",
        label: "Store Supervisor"
    },
    {
        value: "ROLE_STORE_USER",
        label: "Store User"
    }
]

function AdminDetail(props) {

    const dispatch = useDispatch();
    const admin_detail = useSelector(({ adminApp }) => adminApp.admin_detail);
    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);

    useEffect(() => {
        function updateAdminState() {
            const params = props.match.params;
            let admin_detailId = [params]
            if (admin_detailId[0].userId === 'new') {
                localStorage.removeItem('storeObj')
                localStorage.setItem('newForm', true)
                dispatch(Actions.getAdminField());
                dispatch(Actions.newAdmin());
            }
            else {
                localStorage.setItem('newForm', false)
                dispatch(Actions.getAdminField());
                dispatch(Actions.getAdminDetail());
            }
        }

        updateAdminState();
    }, [dispatch, props.match.params]);

    useEffect(() => {
        if (
            (admin_detail.data && !form) ||
            (admin_detail.data && form && admin_detail.data.id !== form.id)
        ) {
            setForm(admin_detail.data);
        }
    }, [form, admin_detail.data, setForm]);

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    function handleChipChange(value, name) {
        setForm(_.set({ ...form }, name, value.map(item => item.value)));
    }

    function fieldfx(form) {
        console.log(admin_detail.field)
        let field = admin_detail.field
        if (field != null) {
            if (localStorage.getItem('newForm') === "true") {
                return (
                    field.map(f =>
                        f.type == "select"
                            ?
                            <FuseChipSelect
                                className="mt-8 mb-24"
                                error={form[f.name] === ''}
                                required
                                value={
                                    form[f.name].map(item => ({
                                        value: item,
                                        label: item
                                    }))
                                }
                                name={f.name}
                                onChange={(value) => handleChipChange(value, f.name)}
                                placeholder="Select multiple"
                                textFieldProps={{
                                    label: f.label,
                                    InputLabelProps: {
                                        shrink: true
                                    },
                                    variant: 'outlined'
                                }}
                                options={suggestions}
                                isMulti
                            />
                            :
                            f.name == "employee_id"
                                ?
                                <TextField
                                    className="mt-8 mb-16"
                                    required
                                    label={f.label}
                                    autoFocus
                                    type={f.type}
                                    id={f.id}
                                    name={f.name}
                                    value={form[f.name]}
                                    onChange={handleChange}
                                    variant="outlined"
                                    fullWidth
                                />
                                :
                                <TextField
                                    className="mt-8 mb-16"
                                    required
                                    label={f.label}
                                    type={f.type}
                                    id={f.id}
                                    name={f.name}
                                    value={form[f.name]}
                                    onChange={handleChange}
                                    variant="outlined"
                                    fullWidth
                                />
                    )
                )
            } else {
                return (
                    field.map(f =>
                        f.type == "select"
                            ?
                            <FuseChipSelect
                                className="mt-8 mb-24"
                                error={form[f.name] === ''}
                                required
                                value={
                                    form[f.name].map(item => ({
                                        value: item,
                                        label: item
                                    }))
                                }
                                name={f.name}
                                onChange={(value) => handleChipChange(value, f.name)}
                                placeholder="Select multiple"
                                textFieldProps={{
                                    label: f.label,
                                    InputLabelProps: {
                                        shrink: true
                                    },
                                    variant: 'outlined'
                                }}
                                options={suggestions}
                                isMulti
                            />
                            :
                            f.name == "password"
                                ?
                                <TextField
                                    className="mt-8 mb-16"
                                    label={f.label}
                                    type={f.type}
                                    id={f.id}
                                    name={f.name}
                                    value={form[f.name]}
                                    onChange={handleChange}
                                    variant="outlined"
                                    multiline
                                    rows={f.rows}
                                    fullWidth
                                />
                                :
                                <TextField
                                    className="mt-8 mb-16"
                                    error={form[f.name] === ''}
                                    required
                                    label={f.label}
                                    type={f.type}
                                    autoFocus
                                    id={f.id}
                                    name={f.name}
                                    value={form[f.name]}
                                    onChange={handleChange}
                                    variant="outlined"
                                    fullWidth
                                />
                    )
                )
            }
        }
    };

    function canBeSubmitted() {
        if (localStorage.getItem('newForm') === "true") {
            return (
                form.name.length > 0 &&
                !_.isEqual(admin_detail.data, form)
            );
        } else {
            return (
                form.name.length > 0 && form.store_id.length > 0 && form.email.length > 0 &&
                !_.isEqual(admin_detail.data, form)
            );
        }
    }

    function canBeDeleted() {
        var deleteIt = localStorage.getItem('newForm')
        if (deleteIt === "false") {
            return (true);
        } else {
            return (false);
        }
    }

    function saveAdmin(data) {
        console.log(data)
        if (localStorage.getItem('newForm') === "true") {
            dispatch(Actions.register(data)).then(res => {
                if (res === false) {
                    dispatch(Action.openDialog({
                        children: (
                            <React.Fragment>
                                <DialogTitle id="alert-dialog-title">Your session expired</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        You'll be redirected to Login page.
                                            </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={() => close_Login()} color="white">
                                        OK
                                            </Button>
                                </DialogActions>
                            </React.Fragment>
                        )
                    }))
                } else if (res) {
                    props.history.push('/admin');
                }
            }).catch(err => {
                console.error(err);
            });
        } else if (localStorage.getItem('newForm') === "false") {
            dispatch(Actions.updateAdmin(data)).then(res => {
                if (res === false) {
                    dispatch(Action.openDialog({
                        children: (
                            <React.Fragment>
                                <DialogTitle id="alert-dialog-title">Your session expired</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        You'll be redirected to Login page.
                                            </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={() => close_Login()} color="white">
                                        OK
                                            </Button>
                                </DialogActions>
                            </React.Fragment>
                        )
                    }))
                } else if (res) {
                    props.history.push('/admin');
                }
            }).catch(err => {
                console.error(err);
            });
        }

    }

    function deleteAdmin(data) {
        dispatch(Action.closeDialog());
        console.log(data)
        dispatch(Actions.deleteAdmin(data)).then(res => {
            if (res === false) {
                dispatch(Action.openDialog({
                    children: (
                        <React.Fragment>
                            <DialogTitle id="alert-dialog-title">Your session expired</DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    You'll be redirected to Login page.
                                        </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={() => close_Login()} color="white">
                                    OK
                                        </Button>
                            </DialogActions>
                        </React.Fragment>
                    )
                }))
            } else if (res) {
                props.history.push('/admin');
            }
        }).catch(err => {
            console.error(err);
        });
    }

    function close_Login() {
        dispatch(Action.closeDialog())
        props.history.push('/login');
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full text-white">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/admin" color="inherit">
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    Admin
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'New Admin'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Detail</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <div className="flex items-center">
                            <FuseAnimate animation="transition.expandIn" delay={300}>
                                <Button
                                    className="whitespace-no-wrap"
                                    variant="contained"
                                    disabled={!canBeSubmitted()}
                                    onClick={() => saveAdmin(form)}
                                >
                                    Save
                            </Button>
                            </FuseAnimate>&nbsp;&nbsp;&nbsp;&nbsp;
                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Button
                                    className="whitespace-no-wrap"
                                    variant="contained"
                                    disabled={!canBeDeleted()}
                                    onClick={() => dispatch(Action.openDialog({
                                        children: (
                                            <React.Fragment>
                                                <DialogTitle id="alert-dialog-title">Confirm to delete?</DialogTitle>
                                                <DialogContent>
                                                    <DialogContentText id="alert-dialog-description">
                                                        Are you sure you want to delete this account?
                                                    </DialogContentText>
                                                </DialogContent>
                                                <DialogActions>
                                                    <Button onClick={() => dispatch(Action.closeDialog())} color="white">
                                                        Cancel
                                                </Button>
                                                    <Button onClick={() => deleteAdmin(form)} color="white" autoFocus>
                                                        OK
                                                </Button>
                                                </DialogActions>
                                            </React.Fragment>
                                        )
                                    }))}
                                >
                                    Delete
                            </Button>
                            </FuseAnimate>
                        </div>
                    </div>
                )
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="secondary"
                    textColor="secondary"
                    variant="scrollable"
                    scrollButtons="auto"
                    classes={{ root: "w-full h-64" }}
                >
                    <Tab className="h-64 normal-case" label="Admin Info" />
                </Tabs>
            }
            content={
                form && (
                    <div className="p-16 sm:p-24 max-w-2xl">
                        {tabValue === 0 &&
                            (
                                <div>
                                    {fieldfx(form)}
                                    {/* error={form.employee_id === ''} */}
                                </div>
                            )}
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('adminApp', reducer)(AdminDetail);
