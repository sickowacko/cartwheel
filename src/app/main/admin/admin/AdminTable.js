import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableCell, TablePagination, TableRow } from '@material-ui/core';
import { FuseScrollbars } from '@fuse';
import { withRouter } from 'react-router-dom';
import _ from '@lodash';
import AdminTableHead from './AdminTableHead';
import * as Actions from '../store/actions';
import { useDispatch, useSelector } from 'react-redux';

function AdminTable(props) {
    const dispatch = useDispatch();
    const admin = useSelector(({ adminApp }) => adminApp.admin.data);
    const searchText = useSelector(({ adminApp }) => adminApp.admin.searchText);

    const [selected] = useState([]);
    const [data, setData] = useState(admin);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [order, setOrder] = useState({
        direction: 'asc',
        id: null
    });

    const param = {
        token: localStorage.getItem('jwt_access_token')
    }

    useEffect(() => {
        dispatch(Actions.getAdmin(param));
    }, [dispatch]);

    useEffect(() => {
        setData(searchText.length === 0 ? admin : _.filter(admin, item => item.name.toLowerCase().includes(searchText.toLowerCase())))
    }, [admin, searchText]);

    function handleRequestSort(event, property) {
        const id = property;
        let direction = 'desc';

        if (order.id === property && order.direction === 'desc') {
            direction = 'asc';
        }

        setOrder({
            direction,
            id
        });
    }

    function handleSelectAllClick(event) {
        // if ( event.target.checked )
        // {
        //     setSelected(data.map(n => n.id));
        //     return;
        // }
        // setSelected([]);
    }

    function handleClick(item) {

        let formData = [{
            'id': item.id,
            'employee_id': item.employee_id,
            'store_id': item.store_id,
            'email': item.email,
            'password': item.password,
            'name': item.name,
            'roles': item.roles
        }]
        localStorage.setItem('id', item.id)
        localStorage.setItem('storeObj', JSON.stringify(formData))
        props.history.push('admin_detail/' + item.employee_id);
    }

    // function handleCheck(event, id) {
    //     const selectedIndex = selected.indexOf(id);
    //     let newSelected = [];

    //     if (selectedIndex === -1) {
    //         newSelected = newSelected.concat(selected, id);
    //     }
    //     else if (selectedIndex === 0) {
    //         newSelected = newSelected.concat(selected.slice(1));
    //     }
    //     else if (selectedIndex === selected.length - 1) {
    //         newSelected = newSelected.concat(selected.slice(0, -1));
    //     }
    //     else if (selectedIndex > 0) {
    //         newSelected = newSelected.concat(
    //             selected.slice(0, selectedIndex),
    //             selected.slice(selectedIndex + 1)
    //         );
    //     }

    //     setSelected(newSelected);
    // }

    function handleChangePage(event, page) {
        setPage(page);
    }

    function handleChangeRowsPerPage(event) {
        setRowsPerPage(event.target.value);
    }

    return (
        <div className="w-full flex flex-col">

            <FuseScrollbars className="flex-grow overflow-x-auto">

                <Table className="min-w-xl" aria-labelledby="tableTitle">

                    <AdminTableHead
                        numSelected={selected.length}
                        order={order}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={data.length}
                    />

                    <TableBody>
                        {_.orderBy(data, [
                            (o) => {
                                switch (order.id) {
                                    case 'categories':
                                        {
                                            return o.categories[0];
                                        }
                                    default:
                                        {
                                            return o[order.id];
                                        }
                                }
                            }
                        ], [order.direction])
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map(n => {
                                const isSelected = selected.indexOf(n.id) !== -1;
                                return (
                                    <TableRow
                                        className="h-64 cursor-pointer"
                                        hover
                                        role="checkbox"
                                        aria-checked={isSelected}
                                        tabIndex={-1}
                                        key={n.id}
                                        selected={isSelected}
                                        onClick={event => handleClick(n)}
                                    >
                                        <TableCell className="w-48 px-4 sm:px-12" padding="checkbox">
                                            {/* <Checkbox
                                                checked={isSelected}
                                                onClick={event => event.stopPropagation()}
                                                onChange={event => handleCheck(event, n.id)}
                                            /> */}
                                        </TableCell>

                                        <TableCell component="th" scope="row">
                                            {n.name}
                                        </TableCell>

                                        <TableCell component="th" scope="row">
                                            {n.email}
                                        </TableCell>

                                        <TableCell component="th" scope="row">
                                            {n.roles == null ? null : n.roles.join(', ')}
                                            {/* {n.roles == null ? null : n.roles.join(', ').split("ROLE_").join(" ").toLowerCase().split("_").join(" ")} */}
                                        </TableCell>

                                        <TableCell component="th" scope="row">
                                            {n.store_id}
                                        </TableCell>

                                        <TableCell component="th" scope="row">
                                            {n.employee_id}
                                        </TableCell>

                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </FuseScrollbars>

            <TablePagination
                component="div"
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page'
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page'
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    );
}

export default withRouter(AdminTable);