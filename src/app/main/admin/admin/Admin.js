import React from 'react';
import {FusePageCarded} from '@fuse';
import withReducer from 'app/store/withReducer';
import AdminTable from './AdminTable';
import AdminHeader from './AdminHeader';
import reducer from '../store/reducers';

function Admin()
{
    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <AdminHeader/>
            }
            content={
                <AdminTable/>
            }
            innerScroll
        />
    );
}

export default withReducer('adminApp', reducer)(Admin);