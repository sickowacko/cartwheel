import React from 'react';
import {Redirect} from 'react-router-dom';

export const AdminConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/admin',
            component: React.lazy(() => import('./admin/Admin'))
        },
        
        {
            path     : '/admin_detail/:userId?',
            component: React.lazy(() => import('./admin/AdminDetail'))
        },
        {
            path     : '/admin',
            component: () => <Redirect to="/admin"/>
        }
    ]
};