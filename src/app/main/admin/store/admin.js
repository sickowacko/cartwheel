
export const getAdmin = (params) => {

    var temp = localStorage.getItem('jwt_access_token');

    params.token = temp ? temp : false;

    var query = {
        "query": [
            {
                "fn": "getUserList",
                "param": {

                    "offset": 0,
                    "orderKey": ["name"],
                    "order": ["asc"],
                    "filter": []
                }
            }
        ],
        "token": temp,
        "New": true
    };

    return fetch('http://wowsyncpim.appscentral.net/api/admin',
        {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
                "Access-Control-Allow-Headers": ["Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Access-Control-Allow-Origin", "Accept", "Content-Type", "X-Amz-Date", "Authorization", "x-api-key", "X-Amz-Security-Token", "Access-Control-Allow-Credentials"],
                "Access-Control-Allow-Credentials": true,
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " + temp
            },
            body: JSON.stringify(query),
        }
    ).then((response) => {
        return response.json();
    }).then(rslt => {
        if (rslt.code && rslt.code === 401) {
            localStorage.removeItem('jwt_access_token');
            return -2;
        } else if (rslt) {
            return rslt.result;
        } else {
            return false;
        }
    }).catch(err => {
        return false;
    });
}

export const updateAdmin = (params) => {
    
    var temp = localStorage.getItem('jwt_access_token');
    var id = localStorage.getItem('id')

    params.token = temp ? temp : false;

    var query = {
        "query": [
            {
                "fn": "updateUser",
                "param": {

                    "employee_id": params.employee_id,
                    "name": params.name,
                    "email": params.email,
                    "roles": params.roles,
                    "password": params.password,
                    "store_id": params.store_id,
                    "id": id
                }
            }
        ],
        "token": temp,
        "New": true
    };

    console.log(query)

    return fetch('http://wowsyncpim.appscentral.net/api/admin',
        {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
                "Access-Control-Allow-Headers": ["Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Access-Control-Allow-Origin", "Accept", "Content-Type", "X-Amz-Date", "Authorization", "x-api-key", "X-Amz-Security-Token", "Access-Control-Allow-Credentials"],
                "Access-Control-Allow-Credentials": true,
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " + temp
            },
            body: JSON.stringify(query),
        }
    ).then((response) => {
        return response.json();
    }).then(rslt => {
        if (rslt.code && rslt.code === 401) {
            localStorage.removeItem('jwt_access_token');
            localStorage.removeItem('id')
            return -2;
        }
        else if (rslt) {
            localStorage.removeItem('id')
            return rslt;
        } else {
            localStorage.removeItem('id')
            return false;
        }
    }).catch(err => {
        return false;
    });
}

export const register = (params) => {
    
    var temp = localStorage.getItem('jwt_access_token');

    params.token = temp ? temp : false;

    var query = {
        "query": [
            {
                "fn": "createUser",
                "param": {

                    "employee_id": params.employee_id,
                    "name": params.name,
                    "email": params.email,
                    "roles": params.roles,
                    "password": params.password,
                    "store_id": params.store_id
                }
            }
        ],
        "token": temp,
        "New": true
    };

    return fetch('http://wowsyncpim.appscentral.net/api/admin',
        {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
                "Access-Control-Allow-Headers": ["Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Access-Control-Allow-Origin", "Accept", "Content-Type", "X-Amz-Date", "Authorization", "x-api-key", "X-Amz-Security-Token", "Access-Control-Allow-Credentials"],
                "Access-Control-Allow-Credentials": true,
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " + temp
            },
            body: JSON.stringify(query),
        }
    ).then((response) => {
        return response.json();
    }).then(rslt => {
        if (rslt.code && rslt.code === 401) {
            localStorage.removeItem('jwt_access_token');
            return -2;
        } else if (rslt) {
            return true;
        } else {
            return false;
        }
    }).catch(err => {
        return false;
    });
}

export const deleteAdmin = (params) => {

    var temp = localStorage.getItem('jwt_access_token');

    params.token = temp ? temp : false;

    var query = {
        "query": [

            {
                "fn": "deleteUser",
                "param": {
                    "id": params.id
                }
            }
        ],
        "token": temp,
        "New": true
    };

    return fetch('http://wowsyncpim.appscentral.net/api/admin',
        {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
                "Access-Control-Allow-Headers": ["Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Access-Control-Allow-Origin", "Accept", "Content-Type", "X-Amz-Date", "Authorization", "x-api-key", "X-Amz-Security-Token", "Access-Control-Allow-Credentials"],
                "Access-Control-Allow-Credentials": true,
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " + temp
            },
            body: JSON.stringify(query),
        }
    )
        .then((response) => {
            return response.json();
        }).then(rslt => {
            if (rslt.code && rslt.code === 401) {
                localStorage.removeItem('jwt_access_token');
                return -2;
            }
            else if (rslt) {
                return rslt;
            } else {
                return false;
            }
        }).catch(err => {
            return false;
        });
}