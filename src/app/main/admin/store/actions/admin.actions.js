import * as Admin from "../admin";

export const SET_ADMIN_SEARCH_TEXT = 'SET_ADMIN_SEARCH_TEXT';
export const GET_ADMIN = "GET_ADMIN";

export const getAdmin = (params) => {

    var succ = false;
    return dispatch => {
        dispatch({
            "type": "LOADING",
            "payload": true,
        });
        return Admin.getAdmin(params).then((data) => {
            dispatch({
                "type": "LOADING",
                "payload": false,
            });
            if (data === -2) {
                succ = false;
            } else if (data) {
                dispatch({
                    "type": GET_ADMIN,
                    "payload": data[0].users
                });
                succ = true;
            }
            return succ;
        }).catch(err => {
            console.error(err);
        });
    }
}

export function setAdminSearchText(event) {
    return {
        type: SET_ADMIN_SEARCH_TEXT,
        searchText: event.target.value
    }
}