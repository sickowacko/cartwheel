import * as Admin from "../admin";
import axios from 'axios';
import { FuseUtils } from '@fuse';
import { showMessage } from 'app/store/actions/fuse';

export const GET_ADMIN_DETAIL = 'GET_ADMIN_DETAIL';
export const GET_ADMIN_FIELD = 'GET_ADMIN_FIELD';

export function getAdminField() {
    const request = axios.get('/api/test-app/user');

    return (dispatch) => {
        dispatch({
            type: "LOADING",
            payload: true
        })
        request.then((response) =>
            dispatch({
                type: GET_ADMIN_FIELD,
                payload: response.data
            })
        );
    }
}

export function getAdminDetail() {
    var formData = JSON.parse(localStorage.getItem('storeObj'));
    const data = {
        id: formData[0].id,
        employee_id: formData[0].employee_id,
        store_id: formData[0].store_id,
        email: formData[0].email,
        password: formData[0].password,
        name: formData[0].name,
        roles: formData[0].roles
    };

    return {
        type: GET_ADMIN_DETAIL,
        payload: data
    }
}

export const register = (params) => {
    console.log(params)
    var succ = false;
    return dispatch => {
        dispatch({
            type: "LOADING",
            payload: true
        })
        return Admin.register(params).then(res => {
            dispatch({
                type: "LOADING",
                payload: false
            })
            if (res) {
                dispatch(showMessage({ message: 'New Account Registered.' }));
                succ = true;
            } else {
                dispatch(showMessage({ message: 'Register failed, please retry.' }));
            }
            return succ;
        }).catch(err => {
            dispatch({
                type: "LOADING",
                payload: false
            })
            throw err;
        });
    }
}

export const updateAdmin = (params) => {
    console.log(params)
    var succ = false;
    return dispatch => {
        dispatch({
            type: "LOADING",
            payload: true
        })
        return Admin.updateAdmin(params).then((res) => {
            if (res === -2) {
                succ = false;
            } else if (res) {
                dispatch(showMessage({ message: 'Account Info have been updated.' }));
                succ = true;
            } else {
                dispatch(showMessage({ message: 'Update failed, please retry.' }));
            }
            dispatch({
                type: "LOADING",
                payload: false
            })
            return succ;
        }).catch(err => {
            dispatch({
                type: "LOADING",
                payload: false
            })
            throw err;
        });
    }
}

export function newAdmin() {
    const data = {
        employee_id: '',
        store_id: '',
        email: '',
        password: '',
        name: '',
        roles: []
    };

    return {
        type: GET_ADMIN_DETAIL,
        payload: data
    }
}

export const deleteAdmin = (params) => {
    console.log(params)
    var succ = false;
    return dispatch => {
        dispatch({
            type: "LOADING",
            payload: true
        })
        return Admin.deleteAdmin(params).then((res) => {
            if (res === -2) {
                succ = false;
            } else if (res) {
                dispatch(showMessage({ message: 'The account have been deleted.' }));
                succ = true;
            } else {
                dispatch(showMessage({ message: 'Unable to delete the record, please retry.' }));
            }
            dispatch({
                type: "LOADING",
                payload: false
            })
            return succ;
        }).catch(err => {
            dispatch({
                type: "LOADING",
                payload: false
            })
            throw err;
        });
    }
}
