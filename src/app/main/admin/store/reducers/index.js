import {combineReducers} from 'redux';
import admin from './admin.reducer';
import admin_detail from './admin_detail.reducer';

const reducer = combineReducers({
    admin,
    admin_detail,
});

export default reducer;