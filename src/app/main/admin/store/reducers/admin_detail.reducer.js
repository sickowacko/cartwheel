import * as Actions from '../actions';

const initialState = {
    data: null,
    field: null,
};

const admin_detailReducer = function (state = initialState, action) {
    switch (action.type) {

        case Actions.GET_ADMIN_DETAIL:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.GET_ADMIN_FIELD:
            {
                return {
                    ...state,
                    field: action.payload
                };
            }
        default:
            {
                return state;
            }
    }
};

export default admin_detailReducer;