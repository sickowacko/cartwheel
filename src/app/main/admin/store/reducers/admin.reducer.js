import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const adminReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_ADMIN:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_ADMIN_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default adminReducer;