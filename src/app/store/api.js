
import jwtService from '../services/jwtService';

const apiUrl = "http://wowsyncpim.appscentral.net/api/admin";
    
const apiHeader = {    
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
    "Access-Control-Allow-Headers": ["Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Access-Control-Allow-Origin", "Accept", "Content-Type", "X-Amz-Date", "Authorization", "x-api-key", "X-Amz-Security-Token", "Access-Control-Allow-Credentials"],
    "Access-Control-Allow-Credentials": true,
    "Content-Type": "application/json",
    "Accept": "application/json",
    // "Authorization": "Bearer " + temp
}

class Api {   
    static send(method, params, token) {   
        
        apiHeader.Authorization = "Bearer " + token;

        return fetch(apiUrl,
            {
                method: method,
                headers: apiHeader,
                body: JSON.stringify(params)
            }
        )
            .then((response) => {
                return response.json();
            })
            .then((rslt) => {
                if (rslt == null) {
                    return false;
                } else if (rslt.code && rslt.code === 401 ) {
                    localStorage.removeItem('jwt_access_token');                    
                    return false;
                } else {
                    return rslt;
                }
            })
            .catch((err) => {
                throw err;
            })
    }


    static post(params) {
        return jwtService.checkTokenValid()
        .then(rslt=>{
            if(rslt){
                return this.send("POST", params, rslt).then(rslt2 => {                    
                    return rslt2;
                })
            } 
            else {
                jwtService.api_tokenExpired_logout();                
            }
        })
        .catch(err => {           
            return err;
        })
    }

}

export default Api;
