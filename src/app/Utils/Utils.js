export const exportSampleCsv = (arrayObj, filename) => {
    const Papaparse = require("papaparse");
    let csv = Papaparse.unparse(arrayObj);
  
    let blob = new Blob([csv], { type: "text/plain;charset=utf-8" });
  
    let url = URL.createObjectURL(blob)
    let link = document.createElement('a');
    link.setAttribute('href', url);
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    window.URL.revokeObjectURL(url);
  }
  
  export const parseCsvFileToJson = (file, header) => {
    const Papaparse = require("papaparse");
  
    return new Promise(function (resolve, reject){
      Papaparse.parse(file, {
        header: header,
        skipEmptyLines: true,
        complete: (rslt) => { resolve(rslt) }
      });
    });
  }