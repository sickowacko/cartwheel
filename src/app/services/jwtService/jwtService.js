import axios from 'axios';
import jwtDecode from 'jwt-decode';
import FuseUtils from '@fuse/FuseUtils';

// import Api from '../../store/api';

class jwtService extends FuseUtils.EventEmitter {

    init() {
        this.setInterceptors();
        this.handleAuthentication();        
    }

    setInterceptors = () => {
        axios.interceptors.response.use(response => {
            return response;
        }, err => {
            return new Promise((resolve, reject) => {
                if (err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
                    // if you ever get an unauthorized response, logout the user
                    this.emit('onAutoLogout', 'Invalid access_token');
                    this.setSession(null);
                }
                throw err;
            });
        });
    };

    handleAuthentication = () => {      

        let access_token = this.getAccessToken();
        
        if (!access_token) {            
            this.emit('onNoAccessToken');
            return;
        }

        if (this.isAuthTokenValid(access_token)) {              
            this.setSession(access_token);
            this.emit('onAutoLogin', true);
        }
        else {             
            this.setSession(null);            
            this.emit('onAutoLogout', 'access_token expired');
        }
    };

    createUser = (data) => {
        return new Promise((resolve, reject) => {
            axios.post('/api/auth/register', data)
                .then(response => {
                    if (response.data.user) {
                        this.setSession(response.data.access_token);
                        resolve(response.data.user);
                    }
                    else {
                        reject(response.data.error);
                    }
                });
        });
    };

    signInWithEmailAndPassword = (email, password) => {
        const payload = {
            username: email,
            password: password
        }

        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: 'http://wowsyncpim.appscentral.net/api/login/admin',
                data: payload, // you are sending body instead
                headers: {
                    // 'Authorization': `bearer ${token}`,
                    'Content-Type': 'application/json'
                },
            }).then(response => {
                if (response.data.token) {
                    const user = {
                        role: "admin",
                        data: {
                            displayName: email,
                            email: email,
                            settings: {
                                layout: {
                                    style: 'layout1',
                                    config: {
                                        scroll: 'content',
                                        navbar: {
                                            display: true,
                                            folded: true,
                                            position: 'left'
                                        },
                                        toolbar: {
                                            display: true,
                                            style: 'fixed',
                                            position: 'below'
                                        },
                                        footer: {
                                            display: true,
                                            style: 'fixed',
                                            position: 'below'
                                        },
                                        mode: 'fullwidth'
                                    }
                                },
                                customScrollbars: true,
                                theme: {
                                    main: 'tech',
                                    navbar: 'defaultDark',
                                    toolbar: 'defaultDark',
                                    footer: 'defaultDark'
                                }
                            }
                        }
                    }

                    this.setSession(response.data.token);
                    localStorage.setItem('email', email)
                    localStorage.setItem('password', password)
                    resolve(user);
                }
                else {
                    reject(response.statusText);
                }
            });
        });
    };

    signInWithToken = () => {        
        //user reload, check token expire
        const payload = {
            username: localStorage.getItem('email'),
            password: localStorage.getItem('password')
        }

        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: 'http://wowsyncpim.appscentral.net/api/login/admin',
                data: payload, // you are sending body instead
                headers: {
                    // 'Authorization': `bearer ${token}`,
                    'Content-Type': 'application/json'
                },
            }).then(response => {
                if (response.data.token) {
                    const user = {
                        role: "admin",
                        data: {
                            displayName: localStorage.getItem('email'),
                            email: localStorage.getItem('email'),
                            settings: {
                                layout: {
                                    style: 'layout1',
                                    config: {
                                        scroll: 'content',
                                        navbar: {
                                            display: true,
                                            folded: true,
                                            position: 'left'
                                        },
                                        toolbar: {
                                            display: true,
                                            style: 'fixed',
                                            position: 'below'
                                        },
                                        footer: {
                                            display: true,
                                            style: 'fixed',
                                            position: 'below'
                                        },
                                        mode: 'fullwidth'
                                    }
                                },
                                customScrollbars: true,
                                theme: {
                                    main: 'tech',
                                    navbar: 'defaultDark',
                                    toolbar: 'defaultDark',
                                    footer: 'defaultDark'
                                }
                            }
                        }
                    }

                    this.setSession(response.data.token);
                    resolve(user);
                }
                else {
                    reject(response.statusText);
                }
            });
        });
    };

    updateUserData = (user) => {
        return axios.post('/api/auth/user/update', {
            user: user
        });
    };

    setSession = access_token => {              
        if (access_token) {    
            localStorage.setItem('jwt_access_token', access_token);
            // axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
        }
        else {           
            localStorage.removeItem('jwt_access_token');           
            // delete axios.defaults.headers.common['Authorization'];
        }
    };

    logout = () => {
        localStorage.removeItem('email');
        localStorage.removeItem('password');
        this.setSession(null);
    };

    isAuthTokenValid = access_token => {
        if (!access_token) {
            return false;
        }
        const decoded = jwtDecode(access_token);
        const currentTime = Date.now() / 1000;
        if (decoded.exp < currentTime) {            
            localStorage.removeItem('email')
            localStorage.removeItem('password')            
            console.warn('access token expired');
            return false;
        }
        else {
            return true;
        }
    };

    getAccessToken = () => {
        return window.localStorage.getItem('jwt_access_token');
    };
   
    checkTokenValid = () => {
        return new Promise((resolve, reject)=>{            
            let token = localStorage.getItem('jwt_access_token');
            if(token){                     
                
                    const decoded = jwtDecode(token);
                    const currentTime = Date.now() / 1000;
                    //-3590
                    if (decoded.exp < currentTime) {
                        localStorage.removeItem('jwt_access_token');          
                        localStorage.removeItem('email')
                        localStorage.removeItem('password')
                        console.warn('access token expired');
                        resolve(false);
                    }
                    else {
                        resolve(token);
                    }
            
            } else {
                resolve(false);
            }

        })
    }

    api_tokenExpired_logout = () => { 
            this.setSession(null);            
            this.emit('onAutoLogout', 'access_token expired');       
    };
}

const instance = new jwtService();

export default instance;
