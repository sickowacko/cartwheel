import mock from '../mock';
import _ from '@lodash';

const storeDB = {
    store: [
        {
            "storeId": "1",
            "name": "Lazada 1",
            "marketplace": "Lazada"
        },
        {
            "storeId": "2",
            "name": "11street 1",
            "marketplace": "11street"
        },
        {
            "storeId": "3",
            "name": "Shopee 1",
            "marketplace": "Shopee"
        }
    ]
};

mock.onGet('/api/test-app/store').reply(() => {
    return [200, storeDB.store];
});