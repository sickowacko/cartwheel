import mock from './../mock';
// import _ from '@lodash';

const testDB = {
    user: [
        {
            'label': 'Employee ID',
            'type': 'text',
            'id': 'employee_id',
            'name': 'employee_id',
        },
        {
            'label': 'Store ID',
            'type': 'text',
            'id': 'store_id',
            'name': 'store_id',
        },
        {
            'label': 'Email',
            'type': 'text',
            'id': 'email',
            'name': 'email',
        },
        {
            'label': 'Password',
            'type': 'password',
            'id': 'password',
            'name': 'password',
        },
        {
            'label': 'Name',
            'type': 'text',
            'id': 'name',
            'name': 'name',
        },
        {
            'label': 'Roles',
            'type': 'select',
            'id': 'roles',
            'name': 'roles',
        },
    ],
    product: [
        {
            'label': 'SKU',
            'type': 'text',
            'id': 'sku',
            'name': 'sku',
            'rows': 0,
            'placeholder': '',
        },
        {
            'label': 'Name',
            'type': 'text',
            'id': 'name',
            'name': 'name',
            'rows': 0,
            'placeholder': '',
        },
        {
            'label': 'Brand',
            'type': 'select',
            'id': 'brand',
            'name': 'brand',
            'rows': 0,
            'placeholder': '',
        },
        {
            'label': 'Halal',
            'type': 'text',
            'id': 'halal',
            'name': 'halal',
            'rows': 0,
            'placeholder': "",
        },
        {
            'label': 'Function',
            'type': 'text',
            'id': 'function',
            'name': 'function',
            'rows': 5,
            'placeholder': '',
        },
        {
            'label': 'Usage Method',
            'type': 'text',
            'id': 'usage_method',
            'name': 'usage_method',
            'rows': 5,
            'placeholder': '',
        },
        {
            'label': 'Tips 1',
            'type': 'text',
            'id': 'tips_1',
            'name': 'tips_1',
            'rows': 5,
            'placeholder': '',
        },
        {
            'label': 'Tips 2',
            'type': 'text',
            'id': 'tips_2',
            'name': 'tips_2',
            'rows': 5,
            'placeholder': '',
        },
        {
            'label': 'Tips 3',
            'type': 'text',
            'id': 'tips_3',
            'name': 'tips_3',
            'rows': 5,
            'placeholder': '',
        },
        {
            'label': 'Description',
            'type': 'text',
            'id': 'description',
            'name': 'description',
            'rows': 5,
            'placeholder': '',
        },
        // {
        //     'label': 'Categories',
        //     'type': 'select',
        //     'id': 'categories',
        //     'name': 'categories',
        //     'rows': 0,
        //     'placeholder': "Select multiple categories",
        // },
        // {
        //     'label': 'Width',
        //     'type': 'text',
        //     'id': 'width',
        //     'name': 'width',
        //     'rows': 0,
        //     'placeholder': '',
        // },
        // {
        //     'label': 'Height',
        //     'type': 'text',
        //     'id': 'height',
        //     'name': 'height',
        //     'rows': 0,
        //     'placeholder': '',
        // },
        // {
        //     'label': 'Depth',
        //     'type': 'text',
        //     'id': 'depth',
        //     'name': 'depth',
        //     'rows': 0,
        //     'placeholder': '',
        // },
        // {
        //     'label': 'Weight',
        //     'type': 'text',
        //     'id': 'weight',
        //     'name': 'weight',
        //     'rows': 0,
        //     'placeholder': '',
        // },
        // {
        //     'label': 'Tags',
        //     'type': 'select',
        //     'id': 'tags',
        //     'name': 'tags',
        //     'rows': 0,
        //     'placeholder': "Select multiple tags",
        // },

    ],
    tabs: {
        "result": [
            [
                {
                    "label": "Product Info",
                    "icon": "",
                    "childs": [
                        {
                            "field_type": "input",
                            "name": "sku",
                            "label": "SKU",
                            "not_editable": true,
                            "visible_in_grid": true
                        },
                        {
                            "field_type": "input",
                            "name": "name",
                            "label": "Name",
                            "not_editable": false,
                            "visible_in_grid": true
                        },
                        {
                            "field_type": "select",
                            "name": "status",
                            "label": "Status",
                            "not_editable": false,
                            "visible_in_grid": false,
                            "options": [
                                {
                                    "key": "Active",
                                    "value": "0"
                                },
                                {
                                    "key": "Discontinued",
                                    "value": "1"
                                },
                                {
                                    "key": "Deleted",
                                    "value": "2"
                                },
                                {
                                    "key": "Quarantine",
                                    "value": "3"
                                }
                            ]
                        },
                        {
                            "field_type": "manyToManyObjectRelation",
                            "name": "category",
                            "label": "Category",
                            "not_editable": false,
                            "visible_in_grid": true,
                            "classes": [
                                {
                                    "classes": "ProductCategory"
                                }
                            ]
                        },
                        {
                            "field_type": "fieldcollections",
                            "name": "marketplaceAttr",
                            "label": "Marketplace Attributes",
                            "not_editable": false,
                            "visible_in_grid": false,
                            "options": {
                                "lazada": [
                                    {
                                        "type": "panel",
                                        "name": "lazada_general",
                                        "label": "Lazada General",
                                        "childs": [
                                            {
                                                "field_type": "input",
                                                "name": "input1",
                                                "label": "input1",
                                                "not_editable": false,
                                                "visible_in_grid": false
                                            }
                                        ]
                                    }
                                ],
                                "shopee": [
                                    {
                                        "type": "panel",
                                        "name": "general",
                                        "label": "General",
                                        "childs": [
                                            {
                                                "field_type": "input",
                                                "name": "input_1",
                                                "label": "input_1",
                                                "not_editable": false,
                                                "visible_in_grid": false
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "label": "Content",
                    "icon": "",
                    "childs": [
                        {
                            "field_type": "wysiwyg",
                            "name": "description",
                            "label": "Description",
                            "not_editable": false,
                            "visible_in_grid": false
                        },
                        {
                            "field_type": "manyToOneRelation",
                            "name": "brand",
                            "label": "Brand",
                            "not_editable": false,
                            "visible_in_grid": false,
                            "classes": [
                                {
                                    "classes": "Brand"
                                }
                            ]
                        },
                        {
                            "field_type": "input",
                            "name": "base_image",
                            "label": "Base_Image",
                            "not_editable": false,
                            "visible_in_grid": false
                        },
                        {
                            "field_type": "input",
                            "name": "thumbnail_image",
                            "label": "Thumbnail Image",
                            "not_editable": false,
                            "visible_in_grid": false
                        }
                    ]
                },
                {
                    "label": "Related",
                    "icon": "",
                    "childs": [
                        {
                            "field_type": "manyToManyObjectRelation",
                            "name": "related",
                            "label": "Related",
                            "not_editable": false,
                            "visible_in_grid": false,
                            "classes": [
                                {
                                    "classes": "Product"
                                }
                            ]
                        },
                        {
                            "field_type": "manyToManyObjectRelation",
                            "name": "upsell",
                            "label": "Upsell",
                            "not_editable": false,
                            "visible_in_grid": false,
                            "classes": [
                                {
                                    "classes": "Product"
                                }
                            ]
                        }
                    ]
                },
                {
                    "label": "Additional Info",
                    "icon": "",
                    "childs": [
                        {
                            "type": "fieldcontainer",
                            "name": "shop_field_con",
                            "label": "Shop Field Container",
                            "childs": [
                                {
                                    "field_type": "input",
                                    "name": "shop_name",
                                    "label": "Shop Name",
                                    "not_editable": false,
                                    "visible_in_grid": false
                                }
                            ]
                        },
                        {
                            "type": "fieldset",
                            "name": "laz_fieldset",
                            "label": "Laz Fieldset",
                            "childs": [
                                {
                                    "field_type": "input",
                                    "name": "laz_name",
                                    "label": "Laz Name",
                                    "not_editable": false,
                                    "visible_in_grid": false
                                }
                            ]
                        }
                    ]
                },
                {
                    "label": "Test",
                    "icon": "",
                    "childs": [
                        {
                            "type": "accordion",
                            "name": "meta_info",
                            "label": "",
                            "childs": [
                                {
                                    "type": "panel",
                                    "name": "panel1",
                                    "label": "Panel1",
                                    "childs": [
                                        {
                                            "field_type": "table",
                                            "name": "barcode",
                                            "label": "Barcode",
                                            "not_editable": false,
                                            "visible_in_grid": false
                                        },
                                        {
                                            "field_type": "input",
                                            "name": "url_key",
                                            "label": "url_key",
                                            "not_editable": false,
                                            "visible_in_grid": false
                                        },
                                        {
                                            "field_type": "input",
                                            "name": "product_name_long",
                                            "label": "Product Name (Long)",
                                            "not_editable": false,
                                            "visible_in_grid": false
                                        }
                                    ]
                                },
                                {
                                    "type": "panel",
                                    "name": "panel2",
                                    "label": "Panel2",
                                    "childs": [
                                        {
                                            "field_type": "input",
                                            "name": "testinput",
                                            "label": "Test Input",
                                            "not_editable": false,
                                            "visible_in_grid": false
                                        }
                                    ]
                                },
                                {
                                    "type": "panel",
                                    "name": "panel3",
                                    "label": "Panel3",
                                    "childs": [
                                        {
                                            "field_type": "textarea",
                                            "name": "testinput2",
                                            "label": "Test Input 2",
                                            "not_editable": false,
                                            "visible_in_grid": false
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        ]
    }
};

mock.onGet('/api/test-app/user').reply(() => {
    return [200, testDB.user];
});

mock.onGet('/api/test-app/product').reply(() => {
    return [200, testDB.product];
});

mock.onGet('/api/test-app/product_tabs').reply(() => {
    return [200, testDB.tabs];
});