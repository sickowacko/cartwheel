import mock from '../mock';
import _ from '@lodash';

const mapDB = {
    user: [
        {
            'label': 'Employee ID',
            'type': 'text',
            'id': 'employee_id',
            'name': 'employee_id',
        },
        {
            'label': 'Store ID',
            'type': 'text',
            'id': 'store_id',
            'name': 'store_id',
        },
        {
            'label': 'Email',
            'type': 'text',
            'id': 'email',
            'name': 'email',
        },
        {
            'label': 'Password',
            'type': 'password',
            'id': 'password',
            'name': 'password',
        },
        {
            'label': 'Name',
            'type': 'text',
            'id': 'name',
            'name': 'name',
        },
        {
            'label': 'Roles',
            'type': 'select',
            'id': 'roles',
            'name': 'roles',
        },
    ],    
    lazada: [				 
        {	
            "name": "Status",
            "label": "Status",						
            "isMandatory": "1",						
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "sku"																
        
        },
        {						
            "name": "Images",
            "label": "Images",						
            "isMandatory": "0",						
            "inputType":"text",
            "valFormat": "array",
            "attributeType": "sku"												
        
        },					
        {						
            "name": "SkuId",
            "label": "SkuId",						
            "isMandatory": "1",						
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "sku"											
        
        },
        {
            "name": "Url",
            "label": "Url",						
            "isMandatory": "0",						
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "sku"				
        
        },
        {
            "name": "name",
            "label": "Name",
            "isMandatory": "1",						
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "normal"				
        },
        {
            "name": "short_description",
            "label": "Short Description",
            "isMandatory": "1",	
            "inputType":"richText",
            "valFormat": "string",	
            "attributeType": "normal"
        },
        {
            "name": "description",
            "label": "Description",
            "isMandatory": "0",	
            "inputType":"richText",
            "valFormat": "string",	
            "attributeType": "normal",		
            "inputName": "optional description",				
            "fieldtype": "input"
            
        },
        {
            "name": "brand",
            "label": "Brand",
            "isMandatory": "1",
            "inputType":"singleSelect",
            "valFormat": "string",	
            "attributeType": "normal",
            "inputName": "brand",				
            "fieldtype": "input"
            
        },				
        {
            "name": "barcode_ean",
            "label": "Barcode",
            "isMandatory": "0",
            "inputType":"text",	
            "valFormat": "string",						
            "attributeType": "sku",
            "inputName": "barcode",				
            "fieldtype": "table"
        }            
    ], 
    shopee: [				 
        {	
            "name": "status",
            "label": "Status",	
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "sku"																
        
        },
        {						
            "name": "images",
            "label": "Images",						
            "isMandatory": "0",						
            "inputType":"text",
            "valFormat": "array",
            "attributeType": "sku"												
        
        },					
        {						
            "name": "item_sku",
            "label": "Item Sku",						
            "isMandatory": "1",						
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "sku"											
        
        },
        {
            "name": "name",
            "label": "Name",
            "isMandatory": "1",						
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "normal"				
        },
        {
            "name": "description",
            "label": "Description",
            "isMandatory": "0",	
            "inputType":"richText",
            "valFormat": "string",	
            "attributeType": "normal",		
            "inputName": "optional description",				
            "fieldtype": "input"
            
        },
        {	
            "name": "category_id",
            "label": "Category Id",	
            "inputType":"text",
            "valFormat": "string",
            "attributeType": "sku"																
        
        }            
    ], 
    ownField: [
        {
            "field_type": "input",
            "name": "sku",
            "label": "SKU",
            "not_editable": true,
            "visible_in_grid": true
        },
        {
            "field_type": "input",
            "name": "name",
            "label": "Name",
            "not_editable": false,
            "visible_in_grid": true
        },
        {
            "field_type": "select",
            "name": "status",
            "label": "Status",
            "not_editable": false,
            "visible_in_grid": false,
            "options": [
                {
                    "key": "Active",
                    "value": "0"
                },
                {
                    "key": "Discontinued",
                    "value": "1"
                },
                {
                    "key": "Deleted",
                    "value": "2"
                },
                {
                    "key": "Quarantine",
                    "value": "3"
                }
            ]
        },
        {
            "field_type": "manyToManyObjectRelation",
            "name": "category",
            "label": "Category",
            "not_editable": false,
            "visible_in_grid": true,
            "classes": [
                {
                    "classes": "ProductCategory"
                }
            ]
        },
        {
            "field_type": "objectbricks",
            "name": "lazada",
            "label": "Lazada",
            "not_editable": false,
            "visible_in_grid": false
        },
        {
            "field_type": "wysiwyg",
            "name": "description",
            "label": "Description",
            "not_editable": false,
            "visible_in_grid": false
        },
        {
            "field_type": "manyToOneRelation",
            "name": "brand",
            "label": "Brand",
            "not_editable": false,
            "visible_in_grid": false,
            "classes": [
                {
                    "classes": "Brand"
                }
            ]
        },
        {
            "field_type": "input",
            "name": "base_image",
            "label": "Base_Image",
            "not_editable": false,
            "visible_in_grid": false
        },
        {
            "field_type": "input",
            "name": "thumbnail_image",
            "label": "Thumbnail Image",
            "not_editable": false,
            "visible_in_grid": false
        },
        {
            "field_type": "manyToManyObjectRelation",
            "name": "related",
            "label": "Related",
            "not_editable": false,
            "visible_in_grid": false,
            "classes": [
                {
                    "classes": "Product"
                }
            ]
        },
        {
            "field_type": "manyToManyObjectRelation",
            "name": "upsell",
            "label": "Upsell",
            "not_editable": false,
            "visible_in_grid": false,
            "classes": [
                {
                    "classes": "Product"
                }
            ]
        },
        {
            "type": "fieldcontainer",
            "name": "shop_field_con",
            "label": "Shop Field Container",
            "childs": [
                {
                    "field_type": "input",
                    "name": "shop_name",
                    "label": "Shop Name",
                    "not_editable": false,
                    "visible_in_grid": false
                }
            ]
        },
        {
            "type": "fieldset",
            "name": "laz_fieldset",
            "label": "Laz Fieldset",
            "childs": [
                {
                    "field_type": "input",
                    "name": "laz_name",
                    "label": "Laz Name",
                    "not_editable": false,
                    "visible_in_grid": false
                }
            ]
        },
        {
            "field_type": "table",
            "name": "barcode",
            "label": "Barcode",
            "not_editable": false,
            "visible_in_grid": false
        },
        {
            "field_type": "input",
            "name": "url_key",
            "label": "url_key",
            "not_editable": false,
            "visible_in_grid": false
        },
        {
            "field_type": "input",
            "name": "product_name_long",
            "label": "Product Name (Long)",
            "not_editable": false,
            "visible_in_grid": false
        }
    ],
    // mapField: [        
    //     {
    //         "Images" : { value: "base_image", key: "Base_Image"}
    //     },
    //     {
    //         "SkuId" :  { value: "sku", key: "SKU"}
    //     },
    //     {
    //         "Url": {value: "url_key", key: "url_key"}
    //     },
    //     {
    //         "name": {value: "name", key: "Name"}
    //     },
    //     {
    //         "short_description" : {value: "description", key: "Description"}
    //     },
    //     {
    //         "brand": {value: "brand", key: "Brand"}
    //     },
    //     {
    //         "barcode_ean": {value: "barcode", key: "Barcode"}	
    //     }, 
    //     {
    //         "Status" : { value: "status", key: "Status"}
    //     }        
    // ],
    mapField: [        
        {
            "Images" : { value: "base_image", key: "Base_Image"}
        },
        {
            "SkuId" :  { value: "sku", key: "SKU"}
        },
        {
            "Url": {value: "url_key", key: "url_key"}
        },
        {
            "name": {value: "name", key: "Name"}
        },
        {
            "short_description" : {value: "description", key: "Description"}
        },
        {
            "brand": {value: "brand", key: "Brand"}
        },
        {
            "barcode_ean": {value: "barcode", key: "Barcode"}	
        }, 
        {
            "Status" : { value: "status", key: "Status"}
        }        
    ]          
};

mock.onGet('/api/test-app/user').reply(() => {
    return [200, mapDB.user];
});

mock.onGet('/api/test-app/lazada').reply(() => {
    return [200, mapDB.lazada];
});

mock.onGet('/api/test-app/shopee').reply(() => {
    return [200, mapDB.shopee];
});

mock.onGet('/api/test-app/ownField').reply(() => {
    return [200, mapDB.ownField];
});

mock.onGet('/api/test-app/mapField').reply(() => {
    return [200, mapDB.mapField];
});