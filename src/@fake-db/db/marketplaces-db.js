import mock from '../mock';
import _ from '@lodash';
import {blue, blueGrey, green, grey} from '@material-ui/core/colors';

const marketplaceDb = {
    category: [
        {
            'id'   : 1,
            'value': 'ecomerce',
            'label': 'Ecommerce Platforms',
            'color': blue[500]
        },
        {
            'id'   : 2,
            'value': 'hk',
            'label': 'Marketplace : Hong Kong',
            'color': green[500]
        },
        {
            'id'   : 3,
            'value': 'idn',
            'label': 'Marketplace : Indonesia',
            'color': green[500]
        },
        {
            'id'   : 4,
            'value': 'my',
            'label': 'Marketplace : Malaysia',
            'color': green[500]
        },
        {
            'id'   : 5,
            'value': 'sg',
            'label': 'Marketplace : Singapore',
            'color': green[500]
        },
        {
            'id'   : 6,
            'value': 'th',
            'label': 'Marketplace : Thailand',
            'color': green[500]
        },
        {
            'id'   : 7,
            'value': 'vn',
            'label': 'Marketplace : Vietnam',
            'color': green[500]
        },
        {
            'id'   : 8,
            'value': 'data feed',
            'label': 'Product Data Feed',
            'color': grey[500]
        }
        
    ],
    marketplaces   : [
        {
            'id'         : '1',
            'name'       : 'Magento',
            'slug'       : 'basics-of-angular',
            'description': 'Hello',
            'category'   : 'ecomerce',
            'length'     : 30,
            'totalSteps' : 2,
            'activeStep' : 0,
            'updated'    : 'Oct 16, 2017',
            'avatar'     : 'assets/images/avatars/Abbott.jpg'
        },
        {
            'id'         : '2',
            'name'       : 'Shopify',
            'slug'       : 'basics-of-typeScript',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'ecomerce',
            'length'     : 60,
            'totalSteps' : 2,
            'activeStep' : 1,
            'updated'    : 'Nov 01, 2017',
            'avatar'  : 'assets/images/avatars/Arnold.jpg',
        },
        {
            'id'         : '3',
            'name'       : 'Woo Commerce',
            'slug'       : 'shopee-n-quick-settings',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'ecomerce',
            'length'     : 120,
            'totalSteps' : 2,
            'activeStep' : 2,
            'updated'    : 'Jun 28, 2017',
            'avatar'  : 'assets/images/avatars/Barrera.jpg'
        },
        {
            'id'    : '21',
            'name'       : 'Lazada Malaysia',
            'slug'       : 'keep-sensitive-data-safe-and-private',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 45,
            'totalSteps' : 2,
            'activeStep' : 2,
            'updated'    : 'Jun 28, 2017',
            'avatar'  : 'assets/images/avatars/Blair.jpg',
        },
        {
            'id'    : '22',
            'name'       : 'Shopee Malaysia',
            'slug'       : 'building-a-grpc-service-with-java',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 30,
            'totalSteps' : 3,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'    : '23',
            'name'       : '11 Street Malaysia',
            'slug'       : 'build-a-pwa-using-workbox',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 120,
            'totalSteps' : 11,
            'activeStep' : 8,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '24',
            'name'      : 'PG Mall Malaysia',
            'slug'       : 'build-an-app-for-the-google-assistant-with-lelong-and-dialogflow',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 30,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '25',
            'name'      : 'Lelong Malaysia',
            'slug'       : 'prestoMall-functions-for-lelong',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 7,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '26',
            'name'      : 'Zalora Malaysia',
            'slug'       : 'manage-your-pivotal-prestoMall-foundry-apps-using-apigee-Edge',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 5,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '27',
            'name'      : 'Etsy Malaysia',
            'your'       : 'building-beautiful-uis-with-flutter',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'my',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '31',
            'name'      : 'Lazada',
            'slug'       : 'prestoMall-firestore',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '32',
            'name'      : 'Shopee',
            'slug'       : 'customize-network-topology-with-subnetworks',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '33',
            'name'      : '11 Street',
            'slug'       : 'looking-at-campaign-finance-with-bigquery',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 60,
            'totalSteps' : 11,
            'activeStep' : 3,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '34',
            'name'      : 'PG Mall',
            'slug'       : 'lelong-shopee',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '35',
            'name'      : 'Lelong',
            'slug'       : 'simulating-a-thread-network-using-openthread',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 1,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '36',
            'name'      : 'Zalora',
            'slug'       : 'your-first-progressive-web-app',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 30,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '37',
            'name'      : 'Etsy',
            'slug'       : 'launch-prestoMall-datalab',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'sg',
            'length'     : 60,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '41',
            'name'      : 'Facebook Store',
            'slug'       : 'launch-prestoMall-datalab',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'data feed',
            'length'     : 60,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'id'         : '42',
            'name'      : 'Google',
            'slug'       : 'launch-prestoMall-datalab',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'data feed',
            'length'     : 60,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        // {
        //     'id'         : '1542e43dfaae6ebf226',
        //     'name'      : 'Personalize Your iOS App with lelong User Management',
        //     'slug'       : 'personalize-your-ios-app-with-lelong-user-management',
        //     'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        //     'category'   : 'lelong',
        //     'length'     : 90,
        //     'totalSteps' : 11,
        //     'activeStep' : 11,
        //     'updated'    : 'Jun 28, 2017'
        // }
    ],

    form : [
        {
            "label": "Marketplace Info",
            "icon": "",
            "childs": [            
                {
                    "field_type": "input",
                    "name": "name",
                    "label": "Marketplace Name",
                    "not_editable": false,
                    "visible_in_grid": true
                },
                {
                    "field_type": "input",
                    "name": "api_token",
                    "label": "Api Token",
                    "not_editable": false,
                    "visible_in_grid": true
                },
                {
                    "field_type": "select",
                    "name": "category",
                    "label": "Categry",
                    "not_editable": false,
                    "visible_in_grid": false,
                    "options": [
                        {
                            "key": "Lazada",
                            "value": "1"
                        },
                        {
                            "key": "Shopee",
                            "value": "2"
                        },
                        {
                            "key": "Presto",
                            "value": "3"
                        },
                        {
                            "key": "Lelong",
                            "value": "4"
                        }
                    ]
                }
            ]
        },
        {
            "label": "Extra",
            "icon": "",
            "childs": [
                {
                    "field_type": "wysiwyg",
                    "name": "description",
                    "label": "Description",
                    "not_editable": false,
                    "visible_in_grid": false
                },
                {
                    "field_type": "manyToOneRelation",
                    "name": "brand",
                    "label": "Brand",
                    "not_editable": false,
                    "visible_in_grid": false,
                    "classes": [
                        {
                            "classes": "Brand"
                        }
                    ]
                },
                {
                    "field_type": "input",
                    "name": "base_image",
                    "label": "Base_Image",
                    "not_editable": false,
                    "visible_in_grid": false
                },
                {
                    "field_type": "input",
                    "name": "thumbnail_image",
                    "label": "Thumbnail Image",
                    "not_editable": false,
                    "visible_in_grid": false
                }
            ]
        }    
    ]
};

mock.onGet('/api/marketplaces/category').reply(() => {
    return [200, marketplaceDb.category];
});

mock.onGet('/api/marketplaces/marketplace').reply(() => {
    return [200, marketplaceDb.marketplaces];
});

mock.onGet('/api/marketplaces/store').reply((request) => {
    const {id} = request.params;
    const response = _.find(marketplaceDb.store, {id: id});
    return [200, response];
});

mock.onGet('/api/marketplaces/form').reply(() => {
    
    // const response = marketplaceDb.form.map(ind=> ind.label);
    return [200, marketplaceDb.form];
});

mock.onPost('/api/marketplaces/marketplace/save').reply((request) => {
    const data = JSON.parse(request.data);
    let store = null;

    marketplaceDb.store = marketplaceDb.store.map(_store => {
        if ( _store.id === data.id )
        {
            store = data;
            return store
        }
        return _store;
    });

    if ( !store )
    {
        store = data;
        marketplaceDb.store = [
            ...marketplaceDb.store,
            store
        ]
    }

    return [200, store];
});

mock.onPost('/api/marketplaces/marketplace/update').reply((request) => {
    const data = JSON.parse(request.data);
    marketplaceDb.store = marketplaceDb.store.map(_store => {
        if ( _store.id === data.id )
        {
            return _.merge(_store, data);
        }
        return _store;
    });

    return [200, data];
});
