import mock from '../mock';
import _ from '@lodash';
import {orange, blue, blueGrey, amber} from '@material-ui/core/colors';

const marketplaceDb = {
    marketplaces: [
        {
            'id'   : 1,
            'value': 'lazada',
            'label': 'Lazada',
            'color': blue[500]
        },
        {
            'id'   : 2,
            'value': 'shopee',
            'label': 'Shopee',
            'color': orange[900]
        },
        {
            'id'   : 3,
            'value': 'prestoMall',
            'label': 'PrestoMall',
            'color': amber[700]
        },
        {
            'id'   : 4,
            'value': 'lelong',
            'label': 'Lelong',
            'color': blueGrey[500]
        }
    ],
    store   : [
        {
            'storeId'    : '1',
            'name'       : 'Lazada 1',
            'slug'       : 'basics-of-angular',
            'description': 'Hello',
            'category'   : 'lazada',
            'length'     : 30,
            'totalSteps' : 2,
            'activeStep' : 0,
            'updated'    : 'Oct 16, 2017',
            'avatar'  : 'assets/images/avatars/Abbott.jpg'
        },
        {
            'storeId'    : '154588a0864d2881124',
            'name'       : 'Lazada 2',
            'slug'       : 'basics-of-typeScript',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lazada',
            'length'     : 60,
            'totalSteps' : 2,
            'activeStep' : 1,
            'updated'    : 'Nov 01, 2017',
            'avatar'  : 'assets/images/avatars/Arnold.jpg',
        },
        {
            'storeId'    : '3',
            'name'       : 'Shopee 1',
            'slug'       : 'shopee-n-quick-settings',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'shopee',
            'length'     : 120,
            'totalSteps' : 2,
            'activeStep' : 2,
            'updated'    : 'Jun 28, 2017',
            'avatar'  : 'assets/images/avatars/Barrera.jpg'
        },
        {
            'storeId'    : '15453a06c08fb021776',
            'name'       : 'Shopee 2',
            'slug'       : 'keep-sensitive-data-safe-and-private',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'shopee',
            'length'     : 45,
            'totalSteps' : 2,
            'activeStep' : 2,
            'updated'    : 'Jun 28, 2017',
            'avatar'  : 'assets/images/avatars/Blair.jpg',
        },
        {
            'storeId'    : '2',
            'name'       : '11 street 1',
            'slug'       : 'building-a-grpc-service-with-java',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'prestoMall',
            'length'     : 30,
            'totalSteps' : 3,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'    : '1542d75d929a603125',
            'name'       : 'Build a PWA Using Workbox',
            'slug'       : 'build-a-pwa-using-workbox',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lazada',
            'length'     : 120,
            'totalSteps' : 11,
            'activeStep' : 8,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1543ee3a5b43e0f9f45',
            'name'      : 'Build an App for the Google Assistant with lelong and Dialogflow',
            'slug'       : 'build-an-app-for-the-google-assistant-with-lelong-and-dialogflow',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lelong',
            'length'     : 30,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1543cc4515df3146112',
            'name'      : 'prestoMall Functions for lelong',
            'slug'       : 'prestoMall-functions-for-lelong',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lelong',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 7,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '154398a4770d7aaf9a2',
            'name'      : 'Manage Your Pivotal prestoMall Foundry App\'s Using Apigee Edge',
            'slug'       : 'manage-your-pivotal-prestoMall-foundry-apps-using-apigee-Edge',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'prestoMall',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 5,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '15438351f87dcd68567',
            'name'      : 'Building Beautiful UIs with Flutter',
            'your'       : 'building-beautiful-uis-with-flutter',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lazada',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1544e43dcdae6ebf876',
            'name'      : 'prestoMall Firestore',
            'slug'       : 'prestoMall-firestore',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lelong',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1541ca7af66da284177',
            'name'      : 'Customize Network Topology with Subnetworks',
            'slug'       : 'customize-network-topology-with-subnetworks',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lazada',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '154297167e781781745',
            'name'      : 'Looking at Campaign Finance with BigQuery',
            'slug'       : 'looking-at-campaign-finance-with-bigquery',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'prestoMall',
            'length'     : 60,
            'totalSteps' : 11,
            'activeStep' : 3,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '154537435d5b32bf11a',
            'name'      : 'lelong shopee',
            'slug'       : 'lelong-shopee',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'shopee',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '154204e45a59b168453',
            'name'      : 'Simulating a Thread Network Using OpenThread',
            'slug'       : 'simulating-a-thread-network-using-openthread',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lazada',
            'length'     : 45,
            'totalSteps' : 11,
            'activeStep' : 1,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1541dd1e05dfc439216',
            'name'      : 'Your First Progressive Web App',
            'slug'       : 'your-first-progressive-web-app',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lazada',
            'length'     : 30,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1532dfc67e704e48515',
            'name'      : 'Launch prestoMall Datalab',
            'slug'       : 'launch-prestoMall-datalab',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'prestoMall',
            'length'     : 60,
            'totalSteps' : 11,
            'activeStep' : 0,
            'updated'    : 'Jun 28, 2017'
        },
        {
            'storeId'         : '1542e43dfaae6ebf226',
            'name'      : 'Personalize Your iOS App with lelong User Management',
            'slug'       : 'personalize-your-ios-app-with-lelong-user-management',
            'description': 'Commits that need to be pushed lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'category'   : 'lelong',
            'length'     : 90,
            'totalSteps' : 11,
            'activeStep' : 11,
            'updated'    : 'Jun 28, 2017'
        }
    ]
};

mock.onGet('/api/overallMapping/marketplaces').reply(() => {
    return [200, marketplaceDb.marketplaces];
});

mock.onGet('/api/overallMapping/stores').reply(() => {
    return [200, marketplaceDb.store];
});

mock.onGet('/api/overallMapping/store').reply((request) => {
    const {storeId} = request.params;
    const response = _.find(marketplaceDb.store, {id: storeId});
    return [200, response];
});

mock.onPost('/api/overallMapping/stores/save').reply((request) => {
    const data = JSON.parse(request.data);
    let store = null;

    marketplaceDb.store = marketplaceDb.store.map(_store => {
        if ( _store.storeId === data.storeId )
        {
            store = data;
            return store
        }
        return _store;
    });

    if ( !store )
    {
        store = data;
        marketplaceDb.store = [
            ...marketplaceDb.store,
            store
        ]
    }

    return [200, store];
});

mock.onPost('/api/overallMapping/stores/update').reply((request) => {
    const data = JSON.parse(request.data);
    marketplaceDb.store = marketplaceDb.store.map(_store => {
        if ( _store.storeId === data.storeId )
        {
            return _.merge(_store, data);
        }
        return _store;
    });

    return [200, data];
});
